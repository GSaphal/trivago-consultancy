<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_finances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('tution_fee')->nullable();
            $table->double('admission_fee')->nullable();
            $table->double('annual_charges')->nullable();
            $table->bigInteger('school_id')->index()->unsigned()->nullable();
            $table->foreign('school_id')->references('id')->on('schools')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_finances');
    }
}
