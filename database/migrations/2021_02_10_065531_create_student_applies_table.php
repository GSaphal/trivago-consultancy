<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_applies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id')
                    ->constrained('students')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreignId('school_id')
                    ->constrained('schools')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreignId('payment_id')
                    ->constrained('payments')
                    ->onUpdate('cascade')
                    ->onDelete('cascade')->nullable();
            $table->enum('payment_status',['unpaid','paid'])->default('unpaid');
            $table->enum('application_status',['pending','accepted','rejected'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_applies');
    }
}
