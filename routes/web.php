<?php

use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SchoolController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Apply\StudentApplyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});
Route::get('aboutUs', function () {
    return view('frontend.aboutUs');
});
Route::get('contactUs', function () {
    return view('frontend.contactUs');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('permissions', PermissionController::class);
    Route::get('/home', function () {
        return view('backend.dashboard');
    });
    Route::resource('schools', SchoolController::class);
    Route::resource('students', StudentController::class);

    
    Route::get('student/address', [StudentController::class, 'viewAddress'])->name('students.viewAddress');
    Route::post('student.address', [StudentController::class, 'storeAddress'])->name('students.storeAddress');

    Route::get('student/educations', [StudentController::class, 'viewEducation'])->name('students.viewEducation');
    Route::post('student/educations', [StudentController::class, 'storeEducation'])->name('students.storeEducation');

    Route::get('student/tests', [StudentController::class, 'viewTestScore'])->name('students.viewTestScore');
    Route::post('student/tests', [StudentController::class, 'storeTestScore'])->name('students.storeTestScore');
    Route::get('school/courses/{id}', [SchoolController::class, 'createCourse'])->name('schools.createCourses');
    Route::post('school/courses', [SchoolController::class, 'storeCourse'])->name('schools.storeCourses');
    Route::get('school/features/{id}', [SchoolController::class, 'createFeature'])->name('schools.createFeatures');
    Route::post('school/features', [SchoolController::class, 'storeFeature'])->name('schools.storeFeatures');

    Route::get('school/images/{id}', [SchoolController::class, 'createImage'])->name('schools.createImages');
    Route::get('find-schools', [SchoolController::class, 'findSchool'])->name('schools.findSchools');

    Route::get('student/school', [StudentController::class, 'schoolIndex'])->name('students.schoolIndex');
    Route::get('student/school/view/{school}', [StudentController::class, 'schoolView'])->name('students.schoolView');


    Route::get('schoolImages/{id}', [SchoolController::class, 'createImage'])->name('schools.createImages');
    Route::post('upload', [PhotoController::class, 'upload'])->name('schools.images.upload');
    Route::post('upload/delete', [PhotoController::class, 'delete'])->name('schools.images.delete');
    Route::get('upload/destroy/{data}', [PhotoController::class, 'destroy'])->name('schools.images.destroy');

    // Route::get('apply/{school}',[StudentApplyController::class,'store'])->name('apply.create');
    Route::get('apply/{school}',[StudentApplyController::class,'store'])->name('apply.store');

});
