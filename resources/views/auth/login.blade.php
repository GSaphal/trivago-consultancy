@extends('backend.layouts.auth')
@section('style')
<style>
.brand-wrapper {
  padding-top: 7px;
  padding-bottom: 5px; }
  .brand-wrapper .logo {
    height: 100px; }

.login-section-wrapper {
  display: -webkit-box;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
          flex-direction: column;
  padding: 68px 100px;
  min-height:100vh;
  background-color: #fff; }
  @media (max-width: 991px) {
    .login-section-wrapper {
      padding-left: 50px;
      padding-right: 50px; } }
  @media (max-width: 575px) {
    .login-section-wrapper {
      padding-top: 20px;
      padding-bottom: 20px;
      min-height: 100vh; } }

.login-wrapper {
  width: 500px;
  max-width: 100%;
  padding-top: 24px;
  padding-bottom: 24px; }
  @media (max-width: 575px) {
    .login-wrapper {
      width: 100%; 
 } }
  .login-wrapper label {
  
    font-size: 14px !important;
    font-weight: 500 !important;
    color: #b0adad; }
  .login-wrapper .form-control {
    border-radius: 0 !important;
    padding: 9px 5px  !important;
    min-height: 40px;
    font-size: 14px  !important;
    font-weight: normal; }


  .login-wrapper .login-btn {
  font-family: 'Roboto', sans-serif !important;


    padding: 13px 20px;
    background-color: #8700FF;
    border-radius: 0;
    font-size: 20px;
    font-weight: bold;
    color: #fff;
    margin-bottom: 14px; }
    .login-wrapper .login-btn:hover {
      border: 1px solid #8700FF;
      background-color: #fff;
      color: #8700FF; }
  .login-wrapper a.forgot-password-link {
    color: #080808;
    font-size: 14px;
    text-decoration: underline;
    display: inline-block;
    margin-bottom: 54px; }
    @media (max-width: 575px) {
      .login-wrapper a.forgot-password-link {
        margin-bottom: 16px; } }
  .login-wrapper-footer-text {
    font-size: 16px;
    color: #000;
    margin-bottom: 0; }

.login-title {
  font-size: 30px;

  color: #000;
  font-weight: bold;
  margin-bottom: 25px; }

.login-img {
  height:100vh;
  width: 100%;

  }


</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 login-section-wrapper">
            <div class="brand-wrapper">
                <img src="{{asset('backend/logo.png')}}" alt="logo" class="logo" style="height:250px;text-align:center;" />
            </div>
            <div class="login-wrapper mt-5">
                <h1 class="login-title">Log in</h1>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label htmlFor="email">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label htmlFor="password">Password</label>

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <button type="submit" id="login" class="btn btn-block login-btn">
                        Login
                    </button>
                    <p class="login-wrapper-footer-text pt-3">
                        Already have an account?
                        <a href="{{ route('register') }}" class="text-reset">
                            Register here
                        </a>
                    </p>
                </form>
            </div>
        </div>
        <div class="col-sm-6 px-0 d-none d-sm-block">
            <img src="{{asset('backend/login.jpg')}}" alt="login image" class="login-img" />
        </div>
    </div>
</div>
@endsection