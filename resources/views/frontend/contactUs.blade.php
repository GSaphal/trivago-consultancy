 @extends('frontend.layouts.app')

 @section('content')

 <!-- MAIN CONTENT WRAPPER -->
 <div class="main-content-wrapper">

     <!-- STUNNING HEADER -->
     <section class="crumina-stunning-header section-image-bg-moon">

         <div class="container">
             <!-- STUNNING HEADER CONTENT -->
             <div class="stunning-header-content align-center">

                 <!-- PAGE TITLE -->
                 <h1 class="page-title text-white">Contact Information</h1>
                 <!-- /PAGE TITLE -->

                 <!-- BREADCRUMBS -->
                 <div class="crumina-breadcrumbs">

                     <!-- BREADCRUMBS LIST -->
                     <ul class="breadcrumbs">

                         <!-- BREADCRUMBS ITEM -->
                         <li class="breadcrumbs-item">
                             <a href="index.html">Homepage</a>
                         </li>
                         <!-- /BREADCRUMBS ITEM -->

                         <!-- BREADCRUMBS ITEM -->
                         <li class="breadcrumbs-item trail-end">
                             <span class="crumina-icon">»</span>
                             <span>Contact Information</span>
                         </li>
                         <!-- /BREADCRUMBS ITEM -->

                     </ul>
                     <!-- /BREADCRUMBS LIST -->

                 </div>
                 <!-- /BREADCRUMBS -->

             </div>
             <!-- /STUNNING HEADER CONTENT -->
         </div>

     </section>
     <!-- /STUNNING HEADER -->

     <section class="large-padding section-image-bg-grey">
         <div class="container">

             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <header class="crumina-module crumina-heading">
                         <div class="title-text-wrap">

                             <!-- CRUMINA HEADING TITLE -->
                             <h2 class="heading-title">Have Any Questions?</h2>
                             <!-- /CRUMINA HEADING TITLE -->

                         </div>

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Please contact us using the form and we’ll get back to you as soon as possible.</div>
                         <!-- /CRUMINA HEADING TEXT -->

                     </header>

                     <form class="send-message-form crumina-submit mt-5" method="post" data-nonce="crumina-submit-form-nonce" data-type="standard" action="modules/forms/submit.php">
                         <div class="row">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div class="form-item">
                                     <input class="input--white" name="site" type="text" placeholder="Website URL" required>
                                 </div>
                                 <div class="form-item">
                                     <input class="input--white" name="email" type="email" placeholder="Email Address" required>
                                 </div>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div class="form-item">
                                     <input class="input--white" name="name" type="text" placeholder="Your Full Name" required>
                                 </div>
                                 <div class="form-item">
                                     <input class="input--white" name="phone" type="text" placeholder="Phone Number" required>
                                 </div>
                             </div>
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-item">
                                     <textarea class="input--white" name="message" placeholder="Details" rows="3" required></textarea>
                                 </div>
                                 <div class="inquiry-btn-wrap">
                                     <button class="crumina-button button--green button--l">SEND INQUIRY</button>
                                     <span>Please, let us know any particular things to check and the best time to contact you by phone (if provided).</span>
                                 </div>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>

         </div>
     </section>

     <section class="large-padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <header class="crumina-module crumina-heading">
                         <div class="title-text-wrap">

                             <!-- CRUMINA HEADING TITLE -->
                             <h2 class="heading-title">Get In Touch</h2>
                             <!-- /CRUMINA HEADING TITLE -->

                         </div>

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</div>
                         <!-- /CRUMINA HEADING TEXT -->

                     </header>
                 </div>
             </div>
         </div>
     </section>

     <div class="medium-padding section-image-bg-dark">
         <div class="container">
             <!-- CONTACTS -->
             <div class="contacts">

                 <!-- CONTACTS ITEM -->
                 <div class="contacts-item">
                     <img loading="lazy" class="crumina-icon" src="img/demo-content/icons/icon4.svg" alt="phone">
                     <div class="content">
                         <div class="title c-white">8 800 567.890.11</div>
                         <p class="sub-title c-white">Mon-Fri 9am-6pm</p>
                     </div>
                 </div>
                 <!-- /CONTACTS ITEM -->

                 <!-- CONTACTS ITEM -->
                 <div class="contacts-item">
                     <img loading="lazy" class="crumina-icon" src="img/demo-content/icons/icon5.svg" alt="mail">
                     <div class="content">
                         <a href="mailto:info@topten.com" class="title c-white">info@topten.com</a>
                         <p class="sub-title c-white">online support</p>
                     </div>
                 </div>
                 <!-- /CONTACTS ITEM -->

                 <!-- CONTACTS ITEM -->
                 <div class="contacts-item">
                     <img loading="lazy" class="crumina-icon" src="img/demo-content/icons/icon6.svg" alt="location">
                     <div class="content">
                         <div class="title c-white">Melbourne, Australia</div>
                         <p class="sub-title c-white">795 South Park Avenue</p>
                     </div>
                 </div>
                 <!-- /CONTACTS ITEM -->

             </div>
             <!-- /CONTACTS -->
         </div>
     </div>

     <div class="crumina-module crumina-map height-500" id="map"></div>


     <!-- SUBSCRIBE SECTION -->
     <section class="medium-padding-top section-image-bg-lime">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 col-md-12 mb-4">
                     <h4 class="subscribe-title">Subscribe to our Newsletter</h4>
                     <p class="subscribe-subtitle text-white">
                         <span class="font-weight-bold">Join Our Newsletter</span> & Marketing Communication. We'll send you news and offers.
                     </p>
                     <form class="subscribe-form">
                         <div class="input-btn--inline">
                             <input class="input--white" type="email" placeholder="Your email address">
                             <button type="button" class="crumina-button button--dark button--l">SUBSCRIBE</button>
                         </div>
                     </form>
                 </div>

                 <div class="col-lg-4 d-none d-lg-block mt-auto">
                     <img loading="lazy" src="img/theme-content/icons/08-subscribe.svg" alt="subscibe">
                 </div>
             </div>
         </div>
     </section>
     <!-- /SUBSCRIBE SECTION -->

     <!-- BACK TO TOP -->
     <div class="back-to-top">
         <svg class="crumina-icon">
             <use xlink:href="#icon-back-to-top"></use>
         </svg>
     </div>
     <!-- /BACK TO TOP -->

 </div>
 <!-- /MAIN CONTENT WRAPPER -->


 @endsection