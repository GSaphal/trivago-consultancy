 @extends('frontend.layouts.app')
 @section('styles')
 <style>
     .crumina-main-slider .slider-content+.slider-thumb {
         /* margin-top: -338px; */
     }
 </style>
 @endsection
 @section('content')
 <!-- MAIN CONTENT WRAPPER -->
 <div class="main-content-wrapper">


     <section class="crumina-module crumina-module-slider crumina-main-slider">
         <!--Prev next buttons-->

         <div class="swiper-btn-next">
             <svg class="crumina-icon" width="40" height="30">
                 <use xlink:href="#icon-nav-next"></use>
             </svg>
         </div>

         <div class="swiper-btn-prev">
             <svg class="crumina-icon" width="40" height="30">
                 <use xlink:href="#icon-nav-prev"></use>
             </svg>
         </div>

         <div class="swiper-container" data-effect="fade" data-show-items="1" data-change-handler="thumbsParent" data-prev-next="1" data-autoplay="4000">

             <!-- Additional required wrapper -->
             <div class="swiper-wrapper">
                 <!-- Slides -->
                 <div class="swiper-slide bg-grey-theme">

                     <div class="container">
                         <div class="row align-items-center">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">

                                 <div class="slider-content">

                                     <h2 class="h1 slider-content-title" data-swiper-parallax="-100">Apply to your dream country.</h2>

                                     <p class="slider-content-text" data-swiper-parallax="-200"> All online for free of cost without any service charge.</p>


                                 </div>

                                 <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                     <img src="{{url('frontend/img/demo-content/icons/Slider1.jpg')}}" width="500"  alt="SEO">
                                 </div>

                             </div>

                         </div>
                     </div>

                 </div>

                 <div class="swiper-slide bg-primary-themes">

                     <div class="container">
                         <div class="row align-items-center">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">

                                 <div class="slider-content">

                                     <h2 class="h1 slider-content-title" data-swiper-parallax="-100">Self apply 
                                         <span class="c-white">without spending thousands of money.</span></h2>

                                     <p class="slider-content-text c-white" data-swiper-parallax="-200">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat eleifend option.</p>

                           

                                 </div>

                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                     <img src="{{url('frontend/img/demo-content/icons/Slider2.png')}}" alt="Case">
                                 </div>
                             </div>

                         </div>
                     </div>

                 </div>

                 <div class="swiper-slide bg-red-themes">

                     <div class="container">
                         <div class="row align-items-center">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                 <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                     <img  src="{{url('frontend/img/demo-content/icons/Slider3.png')}}" alt="Case">
                                 </div>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                 <div class="slider-content">

                                     <h2 class="h1 slider-content-title" data-swiper-parallax="-100">Easy/ Quick Apply</h2>

                                     <p class="slider-content-text c-white" data-swiper-parallax="-200">Apply for a University/ College that matches your background, Skills and Interests with our easy search.</p>

                             

                                 </div>

                             </div>

                         </div>
                     </div>

                 </div>

                 <div class="swiper-slide bg-yellow-themes">

                     <div class="container">
                         <div class="row align-items-center">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center">

                                 <div class="slider-content">

                                     <h2 class="h1 slider-content-title" data-swiper-parallax="-100">1100 Universities and 20,000 Courses</h2>

                                     <p class="slider-content-text c-white" data-swiper-parallax="-200">Find more than 1100 universities and 20,000 courses right from your fingertips.</p>

                              

                                 </div>

                                 <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                     <img src="{{url('frontend/img/demo-content/icons/Slider4.png')}}" width="400" alt="SEO">
                                 </div>

                             </div>

                         </div>
                     </div>

                 </div>

                 <div class="swiper-slide bg-green-themes">

                     <div class="container">
                         <div class="row align-items-center">

                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">

                                 <div class="slider-content">

                                     <h2 class="h1 slider-content-title" data-swiper-parallax="-100">Scholarships</h2>

                                     <p class="slider-content-text c-white" data-swiper-parallax="-200">We have thousands of scholarships available.</p>

                                   

                                 </div>

                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <div class="slider-thumb" data-swiper-parallax="-400" data-swiper-parallax-duration="600">
                                     <img  src="{{url('frontend/img/demo-content/icons/Slide5.png')}}" alt="Case">
                                 </div>
                             </div>

                         </div>
                     </div>

                 </div>

             </div>

             <!--Pagination tabs-->

             <div class="slider-slides main-slider-slides">
                 <div class="main-slider-slides-wrap">
                     <div class="slides-item bg-grey-theme swiper-slide-active">
                         <div class="h5 slides-item-title">Apply</div>
                         <div class="slides-item-text">Apply to your dream country.</div>
                         <img  class="slides-item-icon" src="{{url('frontend/img/demo-content/icons/icon34.png')}}"alt="icon">
                     </div>
                     <div class="slides-item bg-primary-themes">
                         <div class="h5 slides-item-title">Self Apply</div>
                         <div class="slides-item-text">No need to spend money.</div>
                         <img class="slides-item-icon"  src="{{url('frontend/img/demo-content/icons/icon35.png')}}"   alt="icon">
                     </div>
                     <div class="slides-item bg-red-themes">
                         <div class="h5 slides-item-title">Easy/ Quick Apply</div>
                         <div class="slides-item-text">Apply to any university.</div>
                         <img class="slides-item-icon" src="{{url('frontend/img/demo-content/icons/icon36.png')}}" alt="icon">
                     </div>
                     <div class="slides-item bg-yellow-themes">
                         <div class="h5 slides-item-title">Universities</div>
                         <div class="slides-item-text">1100 universities.</div>
                         <img class="slides-item-icon" src="{{url('frontend/img/demo-content/icons/icon37.png')}}" alt="icon">
                     </div>
                     <div class="slides-item bg-green-themes">
                         <div class="h5 slides-item-title">Scholarships</div>
                         <div class="slides-item-text"> Thousands of scholarships.</div>
                         <img  class="slides-item-icon" src="{{url('frontend/img/demo-content/icons/icon38.png')}}" alt="icon">
                     </div>
                 </div>
             </div>

         </div>

     </section>



     <section class="large-padding-top section-image-bg-black section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 m-auto">
                     <header class="crumina-module crumina-heading heading--white mb-5 align-center">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h4 class="heading-title element-anime-fadeInUp-js">We have more than 5000 universities and colleges in five different countries</h4>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text element-anime-fadeInUp-js">Find the best destination of your dream</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                     <form class="seo-score-form mb-5">
                         <div class="form-inline-inputs-wrap element-anime-fadeInUp-js">
                             <input class="input--dark" name="site" type="text" placeholder="Why would you like to study?">
                             <input class="input--dark" name="email" type="email" placeholder="Where? Location or University Name">
                         </div>
                         <header class="crumina-module crumina-heading heading--white mb-5 align-center">

                             <div class="heading-text">Not yet decided? View all Countries and Universities here. </div>
                         </header>
                         <button class="crumina-button button--primary button--l element-anime-opacity-js">Search</button>
                     </form>

                 </div>
                 <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 align-center m-auto">

                     <img loading="lazy" src="{{url('frontend/img/demo-content/icons/06-seo-score.svg')}}" alt="Case" class="element-anime-fadeInUp-js">
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding section-anime-js">
         <div class="container">
             <div class="row align-items-end">
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                     <header class="crumina-module crumina-heading mb-4">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title element-anime-fadeInUp-js">Professional & Experienced Guidance</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration element-anime-fadeInUp-js"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text element-anime-fadeInUp-js">We provide a professional and experienced guidance platform with research, data,network and tools to help students in:</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                     <ul class="crumina-list list--standard fill-red-themes mb-4 element-anime-fadeInUp-js">

                         <!-- LIST STANDARD ITEM -->
                         <li>
                             <svg class="crumina-icon" width="12" height="9">
                                 <use xlink:href="#icon-check"></use>
                             </svg>
                             Career Counseling
                             <!-- /LIST STANDARD ITEM -->

                             <!-- LIST STANDARD ITEM -->
                         <li>
                             <svg class="crumina-icon" width="12" height="9">
                                 <use xlink:href="#icon-check"></use>
                             </svg>
                             College/University selection
                         </li>
                         <!-- /LIST STANDARD ITEM -->

                         <!-- LIST STANDARD ITEM -->
                         <li>
                             <svg class="crumina-icon" width="12" height="9">
                                 <use xlink:href="#icon-check"></use>
                             </svg>
                             Admission Assistance </li>
                         <!-- /LIST STANDARD ITEM -->

                         <!-- LIST STANDARD ITEM -->
                         <li>
                             <svg class="crumina-icon" width="12" height="9">
                                 <use xlink:href="#icon-check"></use>
                             </svg>
                             Visa Document & Interview Preparation
                         </li>
                         <li>
                             <svg class="crumina-icon" width="12" height="9">
                                 <use xlink:href="#icon-check"></use>
                             </svg>
                             Pre Depature & Post Arrival Support
                         </li>
                         <!-- /LIST STANDARD ITEM -->

                     </ul>

           
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mt-4 mt-md-0">
                     <img loading="lazy" class="element-anime-opacity-js" src="{{url('frontend/services.PNG')}}" alt="Case">
                 </div>
             </div>
         </div>
     </section>

     <section class="section-image-bg-grey section-anime-js">
         <div class="row no-gutters align-items-center">
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-1 order-md-0 element-anime-opacity-js">
                 <div class="post-thumb format-video">
                     <img loading="lazy" src="{{asset('frontend/img/demo-content/posts/blog15.jpg')}}" alt="Post">
                     <div class="overlay"></div>
                     <a href="https://www.youtube.com/watch?v=bTqVqk7FSmY" class="play-video js-popup-iframe">
                         <img loading="lazy" src="{{asset('frontend/img/theme-content/icons/play.png')}}" alt="play">
                     </a>
                 </div>
             </div>
             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 order-0 order-md-0">
                 <div class="row justify-content-center align-items-center p-5">
                     <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 align-center">
                         <header class="crumina-module crumina-heading mb-4">
                             <!-- CRUMINA HEADING TITLE -->
                             <div class="title-text-wrap">
                                 <h2 class="heading-title element-anime-fadeInUp-js">About Us</h2>
                             </div>
                             <!-- /CRUMINA HEADING TITLE -->

                             <!-- CRUMINA HEADING DECORATION -->
                             <div class="heading-decoration element-anime-fadeInUp-js"></div>
                             <!-- /CRUMINA HEADING DECORATION  -->
                         </header>

                         <p class="mb-4 element-anime-fadeInUp-js">Founded in 2018,Travigo Edu is a US based,Nepal’s First Leading education technology company focused on helping students with their career, skils, knowledge and abroad study dreams.</p>

                         <a href="{{url('/aboutUs')}}" class="crumina-button button--dark button--l element-anime-opacity-js">View More</a>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding bg-mountains section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 m-auto align-center">
                     <header class="crumina-module crumina-heading mb-5">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title element-anime-fadeInUp-js">The way it works</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration element-anime-fadeInUp-js"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text element-anime-fadeInUp-js">Our four easy steps for your success</div>
                         <div class="heading-text element-anime-fadeInUp-js">Change Objective, Strategy, Technology, Analytics to: </div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                 </div>
             </div>
             <div class="row">
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-info-box info-box--column-centered">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="{{asset('frontend/img/demo-content/icons/icon39.svg')}}" alt="Objective">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Step 1</h5>
                             <p class="info-box-text">Search & Decide</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-info-box info-box--column-centered">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="{{asset('frontend/img/demo-content/icons/icon40.svg')}}" alt="Strategy">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Step 2</h5>
                             <p class="info-box-text">Submit Your Applications & Documents</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-info-box info-box--column-centered">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="{{asset('frontend/img/demo-content/icons/icon41.svg')}}" alt="Technology">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Step 3</h5>
                             <p class="info-box-text">Get Accepted & Start the Visa Process</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-info-box info-box--column-centered">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="{{asset('frontend/img/demo-content/icons/icon42.png')}}" alt="Analytics">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Step 4</h5>
                             <p class="info-box-text">Get Your Ticket & Fly</p>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="row mt-5 justify-content-center align-center">
                 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                     <div class="universal-btn-wrapper element-anime-opacity-js">
                         <a href="36_pricing_tables.html" class="crumina-button button--dark button--l">MORE INFO</a>
                         <a href="https://crumina.net/html-templates/" class="crumina-button button--primary button--l">GET STARTED!</a>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <div class="medium-padding section-image-bg-lime section-anime-js">
         <div class="container">
             <div class="row">

                 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-counter-item">
                         <div class="counter-numbers counter c-white">
                             <span class="counter-value" data-count="98" data-duration="1500"></span>
                             <div class="units">%</div>
                         </div>
                         <span class="counter-title">Acceptance Rate</span>
                     </div>
                 </div>

                 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-counter-item">
                         <div class="counter-numbers counter c-white">
                             <span class="counter-value" data-count="4" data-duration="1000"></span>
                         </div>
                         <span class="counter-title">Years of Service</span>
                     </div>
                 </div>

                 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-counter-item">
                         <div class="counter-numbers counter c-white">
                             <span class="counter-value" data-count="10" data-duration="1500"></span>
                             <div class="units">+</div>
                         </div>
                         <span class="counter-title">Highly Qualified Counselers</span>
                     </div>
                 </div>

                 <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-counter-item">
                         <div class="counter-numbers counter c-white">
                             <span class="counter-value" data-count="2000"></span>
                         </div>
                         <span class="counter-title">Satisfied Students</span>
                     </div>
                 </div>

             </div>
         </div>
     </div>

     <section class="large-padding section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 m-auto">
                     <header class="crumina-module crumina-heading mb-5 align-center">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title element-anime-fadeInUp-js">Our Recent Applications</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration element-anime-fadeInUp-js"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <div class="heading-text element-anime-fadeInUp-js">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</div>

                     </header>
                 </div>
             </div>
             <div class="row justify-content-center">
                 <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-case-item">
                         <a href="#" class="case-item-thumb">
                             <img loading="lazy" src="img/demo-content/case-studies/case3.jpg" alt="Case">
                         </a>
                         <div class="case-item-content">
                             <a href="12_project_details_ver_01.html" class="h6 case-item-title">Lookbook Website Page Template</a>
                             <div class="case-item-cat">
                                 <a href="#">Ecommerce</a>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-case-item">
                         <a href="#" class="case-item-thumb">
                             <img loading="lazy" src="img/demo-content/case-studies/case4.jpg" alt="Case">
                         </a>
                         <div class="case-item-content">
                             <a href="13_project_details_ver_02.html" class="h6 case-item-title">The DO Website Hero Templates</a>
                             <div class="case-item-cat">
                                 <a href="#">SEO</a>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-case-item">
                         <a href="#" class="case-item-thumb">
                             <img loading="lazy" src="img/demo-content/case-studies/case5.jpg" alt="Case">
                         </a>
                         <div class="case-item-content">
                             <a href="12_project_details_ver_01.html" class="h6 case-item-title">Fork Food Free XD Template</a>
                             <div class="case-item-cat">
                                 <a href="#">Food & Drinks</a>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-center mt-5 element-anime-opacity-js">
                     <a href="11_case_studies.html" class="crumina-button button--dark button--l">ALL PROJECTS</a>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding bg-yellow-themes section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 col-md-12 mb-4 mb-lg-0 element-anime-fadeInUp-js">
                     <div class="crumina-module crumina-module-slider pagination-bottom-center">
                         <div class="swiper-container" data-show-items="1" data-prev-next="1" data-effect="fade" data-loop="false">

                             <div class="swiper-wrapper">

                                 <div class="swiper-slide">
                                     <div class="crumina-testimonial-item testimonial-item-modern">
                                         <div class="author-avatar" data-swiper-parallax="-50">
                                             <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                         </div>
                                         <div class="testimonial-content">
                                             <h5 class="testimonial-text" data-swiper-parallax-x="-200">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim. </h5>
                                             <div class="author-quote-wrap" data-swiper-parallax="-50">
                                                 <div class="post-author author vcard">
                                                     <div class="author-text">
                                                         <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                         <div class="author-prof">Lead Manager</div>
                                                     </div>
                                                 </div>
                                                 <div class="quote">
                                                     <img loading="lazy" src="img/demo-content/icons/quote-dark.png" alt="Quote">
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-testimonial-item testimonial-item-modern">
                                         <div class="author-avatar" data-swiper-parallax="-50">
                                             <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                         </div>
                                         <div class="testimonial-content">
                                             <h5 class="testimonial-text" data-swiper-parallax-x="-200">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim. </h5>
                                             <div class="author-quote-wrap" data-swiper-parallax="-50">
                                                 <div class="post-author author vcard">
                                                     <div class="author-text">
                                                         <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                         <div class="author-prof">Lead Manager</div>
                                                     </div>
                                                 </div>
                                                 <div class="quote">
                                                     <img loading="lazy" src="img/demo-content/icons/quote-dark.png" alt="Quote">
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-testimonial-item testimonial-item-modern">
                                         <div class="author-avatar" data-swiper-parallax="-50">
                                             <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                         </div>
                                         <div class="testimonial-content">
                                             <h5 class="testimonial-text" data-swiper-parallax-x="-200">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim. </h5>
                                             <div class="author-quote-wrap" data-swiper-parallax="-50">
                                                 <div class="post-author author vcard">
                                                     <div class="author-text">
                                                         <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                         <div class="author-prof">Lead Manager</div>
                                                     </div>
                                                 </div>
                                                 <div class="quote">
                                                     <img loading="lazy" src="img/demo-content/icons/quote-dark.png" alt="Quote">
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                             </div>
                         </div>

                         <!-- If we need pagination -->
                         <div class="swiper-pagination swiper-pagination-dark"></div>
                     </div>
                 </div>

                 <div class="col-lg-5 col-md-12 element-anime-fadeInUp-js">
                     <h3>What Our Clients Say About TopTen</h3>


                     <div class="crumina-module js-animate-icon medium-padding-top">

                         <svg class="crumina-icon c-dark" version="1.1" width="320" height="95" viewBox="0 0 640 190" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                             <g id="id661d7e3efaca5682dc50d402" mask="url(#canvasMask)">
                                 <path id="Step01" d="M 83.295,170 C 118.295,152 120.295,117.003 19.295,174 C -81.705,231 78.295,197 78.295,197 C 78.295,197 98.295,189 115.295,192 M 83.295,170 Z" style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,22.1376,-98.96839999999999)" class="Step01"></path>
                                 <path id="Step02" d="M 147.295,196 C 151.295,202 155.295,207 155.295,207 " style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,-2.5818999999999903,-109.551)" class="Step02"></path>
                                 <path id="Step03" d="M 195.295,144 C 189.295,166 182.295,186 177.295,197 M 195.295,144 Z" style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.7130999999999972,-93.051)" class="Step03"></path>
                                 <path id="Step04" d="M 148.295,141 C 158.295,97 366.315,126.739 178.295,190.999 C 99.295,217.999 196.305,173.086 221.294,166.001 C 243.607,159.676 258.289,162.001 273.289,165.001 " style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.3153999999999826,-93.6399)" class="Step04"></path>
                                 <path id="Step05" d="M 253.295,155 C 245.295,170 241.295,180 248.295,185 M 253.295,155 Z" style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.7128999999999905,-93.051)" class="Step05"></path>
                                 <path id="Step06" d="M 260.295,124 C 259.295,129 263.295,134 263.295,134 M 260.295,124 Z" style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.7130999999999972,-93.051)" class="Step06"></path>
                                 <path id="Step07" d="M 263.295,158 C 257.295,167 254.295,171 255.295,180 C 256.295,189 270.295,170 279.295,164 C 288.597,157.801 312.318,161.029 285.294,183.999 C 280.294,195.999 303.077,163.404 330.077,165.404 M 263.294,158.002 " style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.931100000000015,-92.4558)" class="Step07"></path>
                                 <path id="Step08" d="M 361.295,110 C 334.295,110 300.192,154.686 310.295,184.998 C 311.295,187.998 315.878,175.651 329.293,160 C 335.293,153 329.293,147 327.293,158 C 323.854,176.925 319.842,185.999 329.293,195.999 C 331.353,198.179 333.292,201.997 344.292,196.997 C 351.467,190.191 357.589,177.576 367.29,180.001 C 379.29,183.001 390.29,183.001 402.29,172.001 C 416.467,159.004 406.059,150.423 420.289,152.003 C 447.289,155.003 468.289,145.003 490.289,156.003 C 512.289,167.003 414.227,290.355 333.291,276.004 C 289.258,268.197 511.292,95.006 628.292,99.006 " style="fill: none; stroke: currentColor; stroke-width: 3.0; stroke-opacity: 1.0" transform="matrix(1.0,0.0,0.0,1.0,1.713200000000029,-93.0515)" class="Step08"></path>
                             </g>
                         </svg>

                     </div>
                 </div>

             </div>
         </div>
     </section>



     <section class="large-padding section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col">
                     <header class="crumina-module crumina-heading mb-5">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title element-anime-fadeInUp-js">Latest From the Blog</h2>
                             <a href="14_blog.html" class="read-more element-anime-fadeInUp-js" title="See all Projects">
                                 Read Our Blog
                                 <svg class="crumina-icon" width="15" height="9">
                                     <use xlink:href="#icon-arrow-think"></use>
                                 </svg>
                             </a>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration element-anime-fadeInUp-js"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                     </header>

                     <div class="crumina-module crumina-module-slider pagination-bottom-center">
                         <div class="swiper-container" data-show-items="3" data-prev-next="1">

                             <div class="swiper-wrapper">

                                 <div class="swiper-slide element-anime-fadeInUp-js">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="15_post_details.html" class="h6 post-title entry-title">How To Create A Successful Digital Marketing Blog: Asking Experts</a>

                                         <div class="post-text">
                                             It's not much of a secret that blogs are necessary for the efficient promotion of your business and creating your company's image.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide element-anime-fadeInUp-js">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="16_video_post_format.html" class="h6 post-title entry-title">How Well Do You Know Google Algorithms?? Test Your Knowledge With TopTen</a>

                                         <div class="post-text">
                                             Again and again, you hear about new Google algorithm updates that aim to show the most helpful and relevant content in search results.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide element-anime-fadeInUp-js">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="15_post_details.html" class="h6 post-title entry-title">How To Create A Content Plan</a>

                                         <div class="post-text">
                                             The first rule of the content plan is that there isn't the only correct way to gather topics for the content plan.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="15_post_details.html" class="h6 post-title entry-title">How To Create A Successful Digital Marketing Blog: Asking Experts</a>

                                         <div class="post-text">
                                             It's not much of a secret that blogs are necessary for the efficient promotion of your business and creating your company's image.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="16_video_post_format.html" class="h6 post-title entry-title">How Well Do You Know Google Algorithms?? Test Your Knowledge With TopTen</a>

                                         <div class="post-text">
                                             Again and again, you hear about new Google algorithm updates that aim to show the most helpful and relevant content in search results.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="latest-news-item">
                                         <div class="post-additional-info-wrap">
                                             <div class="post-time-reading">
                                                 <svg class="crumina-icon" width="20" height="20">
                                                     <use xlink:href="#icon-timer"></use>
                                                 </svg>
                                                 March 1, 2020
                                             </div>
                                         </div>

                                         <a href="15_post_details.html" class="h6 post-title entry-title">How To Create A Content Plan</a>

                                         <div class="post-text">
                                             The first rule of the content plan is that there isn't the only correct way to gather topics for the content plan.
                                         </div>

                                         <div class="post-author author vcard">
                                             <div class="author-avatar">
                                                 <img loading="lazy" src="img/demo-content/avatars/author1.png" alt="Author">
                                             </div>
                                             <div class="author-text">
                                                 <a href="#" class="post-author-name fn">Liondekam</a>
                                                 <div class="post-date">November 18, 2019</div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>


                             </div>

                         </div>

                         <!-- If we need pagination -->
                         <div class="swiper-pagination"></div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding bg-grey-theme section-anime-js">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6 col-md-8 col-sm-12 m-auto">
                     <header class="crumina-module crumina-heading mb-5 align-center">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title element-anime-fadeInUp-js">Our Valuable Clients</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration element-anime-fadeInUp-js"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <div class="heading-text element-anime-fadeInUp-js">Qui mutationem consuetudium.</div>

                     </header>
                 </div>
             </div>
             <div class="crumina-module crumina-module-slider navigation-bottom-center">

                 <div class="swiper-btn-next element-anime-opacity-js">
                     <svg class="crumina-icon" width="40" height="30">
                         <use xlink:href="#icon-nav-next"></use>
                     </svg>
                 </div>

                 <div class="swiper-btn-prev element-anime-opacity-js">
                     <svg class="crumina-icon" width="40" height="30">
                         <use xlink:href="#icon-nav-prev"></use>
                     </svg>
                 </div>

                 <div class="swiper-container" data-show-items="4" data-prev-next="1">

                     <div class="swiper-wrapper">

                         <div class="swiper-slide element-anime-opacity-js">
                             <a href="09_our_clients.html" class="crumina-module clients-item">
                                 <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients1.png" alt="Client">
                             </a>
                         </div>

                         <div class="swiper-slide element-anime-opacity-js">
                             <a href="09_our_clients.html" class="crumina-module clients-item">
                                 <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients2.png" alt="Client">
                             </a>
                         </div>

                         <div class="swiper-slide element-anime-opacity-js">
                             <a href="09_our_clients.html" class="crumina-module clients-item">
                                 <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients3.png" alt="Client">
                             </a>
                         </div>

                         <div class="swiper-slide element-anime-opacity-js">
                             <a href="09_our_clients.html" class="crumina-module clients-item">
                                 <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients4.png" alt="Client">
                             </a>
                         </div>

                     </div>

                 </div>

             </div>
         </div>
     </section>

     <!-- SUBSCRIBE SECTION -->
     <section class="medium-padding-top section-image-bg-breez">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 col-md-12 mb-4">
                     <h4 class="subscribe-title">Subscribe to our Newsletter</h4>
                     <p class="subscribe-subtitle text-white">
                         <span class="font-weight-bold">Join Our Newsletter</span> & Marketing Communication. We'll send you news and offers.
                     </p>
                     <form class="subscribe-form">
                         <div class="input-btn--inline">
                             <input class="input--white" type="email" placeholder="Your email address">
                             <button type="button" class="crumina-button button--dark button--l">SUBSCRIBE</button>
                         </div>
                     </form>
                 </div>

                 <div class="col-lg-4 d-none d-lg-block mt-auto">
                     <img loading="lazy" src="img/theme-content/icons/08-subscribe.svg" alt="subscibe">
                 </div>
             </div>
         </div>
     </section>
     <!-- /SUBSCRIBE SECTION -->

     <!-- BACK TO TOP -->
     <div class="back-to-top">
         <svg class="crumina-icon">
             <use xlink:href="#icon-back-to-top"></use>
         </svg>
     </div>
     <!-- /BACK TO TOP -->

 </div>
 <!-- /MAIN CONTENT WRAPPER -->


 @endsection