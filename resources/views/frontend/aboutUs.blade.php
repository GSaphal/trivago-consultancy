 @extends('frontend.layouts.app')

 @section('content')
 <!-- MAIN CONTENT WRAPPER -->
 <div class="main-content-wrapper">

     <!-- STUNNING HEADER -->
     <section class="crumina-stunning-header section-image-bg-blue-light">

         <div class="container">
             <!-- STUNNING HEADER CONTENT -->
             <div class="stunning-header-content align-center">

                 <!-- PAGE TITLE -->
                 <h1 class="page-title text-white ">About our agency</h1>
                 <!-- /PAGE TITLE -->

                 <!-- BREADCRUMBS -->
                 <div class="crumina-breadcrumbs">

                     <!-- BREADCRUMBS LIST -->
                     <ul class="breadcrumbs">

                         <!-- BREADCRUMBS ITEM -->
                         <li class="breadcrumbs-item">
                             <a href="index.html">Homepage</a>
                         </li>
                         <!-- /BREADCRUMBS ITEM -->

                         <!-- BREADCRUMBS ITEM -->
                         <li class="breadcrumbs-item trail-end">
                             <span class="crumina-icon">»</span>
                             <span>About our agency</span>
                         </li>
                         <!-- /BREADCRUMBS ITEM -->

                     </ul>
                     <!-- /BREADCRUMBS LIST -->

                 </div>
                 <!-- /BREADCRUMBS -->

             </div>
             <!-- /STUNNING HEADER CONTENT -->
         </div>

     </section>
     <!-- /STUNNING HEADER -->

     <section class="large-padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <header class="crumina-module crumina-heading mb-3">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title ">Short Story About Our Company</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration "></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                     <p>Mirum est notare quam littera gothica, quam nunc putamus parum, anteposuerit litterarum formas.</p>
                 </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                     <div class="crumina-module crumina-module-slider time-line-slider time-line-slider--slide-rounde">

                         <div class="swiper-container gallery-thumbs swiper-time-line" data-show-items="6" data-loop="0">

                             <div class="swiper-btn-next">
                                 <svg class="crumina-icon" width="40" height="30">
                                     <use xlink:href="#icon-nav-next"></use>
                                 </svg>
                             </div>

                             <div class="swiper-btn-prev">
                                 <svg class="crumina-icon" width="40" height="30">
                                     <use xlink:href="#icon-nav-prev"></use>
                                 </svg>
                             </div>

                             <div class="swiper-wrapper slider-slides time-line-slides">

                                 <div class="swiper-slide slides-item time-active">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2015</a>
                                 </div>

                                 <div class="swiper-slide slides-item">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2016</a>
                                 </div>
                                 <div class="swiper-slide slides-item">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2017</a>
                                 </div>
                                 <div class="swiper-slide slides-item">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2018</a>
                                 </div>
                                 <div class="swiper-slide slides-item">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2019</a>
                                 </div>
                                 <div class="swiper-slide slides-item">
                                     <span class="time-line-decoration"></span>
                                     <a href="#" class="btn--hover-decoration">2020</a>
                                 </div>

                             </div>

                         </div>

                         <div class="swiper-container gallery-top" data-loop="1" data-change-handler="timeParent" data-prev-next="1">

                             <div class="swiper-wrapper">

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <div class="time mb-4">November, 1015</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <div class="time mb-4">November, 1016</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <div class="time mb-4">November, 1017</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <div class="time mb-4">November, 1018</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <div class="time mb-4">November, 1019</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                                             <div class="time mb-4">November, 1020</div>
                                             <h3>Foundation of the Company</h3>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer feugiat scelerisque varius.</p>
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <img loading="lazy" src="img/demo-content/icons/09-story-company.svg" alt="Case">
                                         </div>
                                     </div>
                                 </div>


                             </div>

                         </div>

                     </div>

                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding section-image-bg-lime">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                     <div class="crumina-module crumina-module-slider crumina-slider--vertical pagination-left-center">
                         <div class="swiper-container" data-prev-next="1" data-direction="vertical" data-loop="false">
                             <div class="swiper-wrapper">
                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0">
                                             <h2>We Work for Your Profit</h2>
                                             <p class="c-white mb-4">Codipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer.</p>
                                             <a href="14_blog.html" class="crumina-button button--dark button--l">READ MORE</a>
                                         </div>

                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <img loading="lazy" src="img/demo-content/icons/10-we-work.svg" alt="Case">
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0">
                                             <img loading="lazy" src="img/demo-content/icons/10-we-work.svg" alt="Case">
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <h2>We Work for Your Profit</h2>
                                             <p class="c-white mb-4">Codipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer.</p>
                                             <a href="14_blog.html" class="crumina-button button--dark button--l">READ MORE</a>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0">
                                             <h2>We Work for Your Profit</h2>
                                             <p class="c-white mb-4">Codipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer.</p>
                                             <a href="14_blog.html" class="crumina-button button--dark button--l">READ MORE</a>
                                         </div>

                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <img loading="lazy" src="img/demo-content/icons/10-we-work.svg" alt="Case">
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="row align-items-center">
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0">
                                             <img loading="lazy" src="img/demo-content/icons/10-we-work.svg" alt="Case">
                                         </div>
                                         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                             <h2>We Work for Your Profit</h2>
                                             <p class="c-white mb-4">Codipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus. Pharetra massa massa ultricies mi quis hendrerit. Aliquam sem et tortor consequat id porta. Sed velit dignissim sodales ut. Viverra adipiscing at in tellus integer.</p>
                                             <a href="14_blog.html" class="crumina-button button--dark button--l">READ MORE</a>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <!-- If we need pagination -->
                             <div class="swiper-pagination swiper-pagination-dark"></div>
                         </div>
                     </div>

                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding">
         <div class="container">
             <div class="row">
                 <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 m-auto align-center">
                     <header class="crumina-module crumina-heading mb-5">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title ">Meet with Our Best Experts</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration "></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium.</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                 </div>
             </div>
             <div class="row">
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 ">
                     <div class="crumina-module crumina-teammember-item">
                         <div class="teammember-item-thumb">
                             <img loading="lazy" src="img/demo-content/teammembers/teammember1.png" alt="teammember">
                         </div>
                         <a href="#" class="h5 teammember-author-name">Peter Spenser</a>
                         <div class="teammember-author-prof">Copywriter</div>
                         <div class="socials">
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/facebook.svg" alt="facebook">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/twitter.svg" alt="twitter">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/google.svg" alt="google">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/youtube.svg" alt="youtube">
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 ">
                     <div class="crumina-module crumina-teammember-item">
                         <div class="teammember-item-thumb">
                             <img loading="lazy" src="img/demo-content/teammembers/teammember2.png" alt="teammember">
                         </div>
                         <a href="#" class="h5 teammember-author-name">Angelina Johnson</a>
                         <div class="teammember-author-prof">SEO Specialist</div>
                         <div class="socials">
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/facebook.svg" alt="facebook">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/twitter.svg" alt="twitter">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/google.svg" alt="google">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/youtube.svg" alt="youtube">
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 mb-4 mb-lg-0 ">
                     <div class="crumina-module crumina-teammember-item">
                         <div class="teammember-item-thumb">
                             <img loading="lazy" src="img/demo-content/teammembers/teammember3.png" alt="teammember">
                         </div>
                         <a href="#" class="h5 teammember-author-name">Philip Demarco</a>
                         <div class="teammember-author-prof">Senior Developer</div>
                         <div class="socials">
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/facebook.svg" alt="facebook">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/twitter.svg" alt="twitter">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/google.svg" alt="google">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/youtube.svg" alt="youtube">
                             </a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ">
                     <div class="crumina-module crumina-teammember-item">
                         <div class="teammember-item-thumb">
                             <img loading="lazy" src="img/demo-content/teammembers/teammember4.png" alt="teammember">
                         </div>
                         <a href="#" class="h5 teammember-author-name">James Anderson</a>
                         <div class="teammember-author-prof">Business Analyst</div>
                         <div class="socials">
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/facebook.svg" alt="facebook">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/twitter.svg" alt="twitter">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/google.svg" alt="google">
                             </a>
                             <a class="social-item" href="#">
                                 <img loading="lazy" class="crumina-icon" src="img/theme-content/social-icons/youtube.svg" alt="youtube">
                             </a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding bg-grey-theme">
         <div class="container">
             <div class="row">
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4 mb-md-0">
                     <div class="crumina-module crumina-info-box info-box--standard">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="img/demo-content/icons/icon31.svg" alt="Support">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Excellent Support</h5>
                             <p class="info-box-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna.</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 mb-4 mb-md-0">
                     <div class="crumina-module crumina-info-box info-box--standard">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="img/demo-content/icons/icon32.svg" alt="Awesome Team">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Awesome Team</h5>
                             <p class="info-box-text">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum investigationes demonstraverunt.</p>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                     <div class="crumina-module crumina-info-box info-box--standard">
                         <div class="info-box-thumb">
                             <img loading="lazy" src="img/demo-content/icons/icon33.svg" alt="Faster Performance">
                         </div>
                         <div class="info-box-content">
                             <h5 class="info-box-title">Faster Performance</h5>
                             <p class="info-box-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt laoreet dolore magna.</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding-top section-image-bg-yellow">
         <div class="container">
             <div class="row">
                 <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 m-auto align-center">
                     <header class="crumina-module crumina-heading mb-5">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title ">Our Vision</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>
                 </div>

                 <div class="col-lg-4 col-md-8 col-sm-12 col-xs-12 m-auto align-center ">
                     <img loading="lazy" src="img/demo-content/icons/11-our-vision.svg" alt="Vision">
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding bg-grey-theme">
         <div class="container">
             <div class="row">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <header class="crumina-module crumina-heading mb-5">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title">Our Customers Say</h2>
                             <a href="#" class="read-more" title="See all Projects">
                                 Read All Testimonials
                                 <svg class="crumina-icon" width="15" height="9">
                                     <use xlink:href="#icon-arrow-think"></use>
                                 </svg>
                             </a>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                     </header>

                     <div class="crumina-module crumina-module-slider pagination-bottom-left">
                         <div class="swiper-container" data-show-items="2" data-prev-next="1">

                             <div class="swiper-wrapper">

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim amet luctus venenatis.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author5.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim amet luctus venenatis.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author5.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim amet luctus venenatis.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author5.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author8.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="crumina-module crumina-testimonial-item testimonial-item-arrow">
                                         <h5 class="testimonial-text">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim amet luctus venenatis.</h5>
                                         <div class="author-quote-wrap">
                                             <div class="post-author author vcard">
                                                 <div class="author-avatar">
                                                     <img loading="lazy" src="img/demo-content/avatars/author5.png" alt="Author">
                                                 </div>
                                                 <div class="author-text">
                                                     <a href="#" class="post-author-name fn">Frank Simpson</a>
                                                     <div class="author-prof">Lead Manager</div>
                                                 </div>
                                             </div>
                                             <div class="quote">
                                                 <img loading="lazy" src="img/demo-content/icons/quote.png" alt="Quote">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                             </div>

                         </div>

                         <!-- If we need pagination -->
                         <div class="swiper-pagination"></div>
                     </div>
                 </div>
             </div>
         </div>
     </section>

     <section class="large-padding">
         <div class="container">
             <div class="row align-items-center">
                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4 mb-md-0">
                     <header class="crumina-module crumina-heading mb-4">
                         <!-- CRUMINA HEADING TITLE -->
                         <div class="title-text-wrap">
                             <h2 class="heading-title">PPC Marketing</h2>
                         </div>
                         <!-- /CRUMINA HEADING TITLE -->

                         <!-- CRUMINA HEADING DECORATION -->
                         <div class="heading-decoration"></div>
                         <!-- /CRUMINA HEADING DECORATION  -->

                         <!-- CRUMINA HEADING TEXT -->
                         <div class="heading-text">Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima.</div>
                         <!-- /CRUMINA HEADING TEXT -->
                     </header>

                     <a href="#" class="crumina-button button--dark button--l">VIEW ALL CUSTOMERS</a>
                 </div>
                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 align-center">
                     <a href="09_our_clients.html" class="crumina-module clients-item mb-5">
                         <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients1.png" alt="Client">
                     </a>
                     <a href="09_our_clients.html" class="crumina-module clients-item mb-5">
                         <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients2.png" alt="Client">
                     </a>
                 </div>
                 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 align-center">
                     <a href="09_our_clients.html" class="crumina-module clients-item mb-5">
                         <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients3.png" alt="Client">
                     </a>
                     <a href="09_our_clients.html" class="crumina-module clients-item mb-5">
                         <img loading="lazy" class="crumina-icon" src="img/demo-content/clients/clients4.png" alt="Client">
                     </a>
                 </div>
             </div>
         </div>
     </section>

     <!-- SUBSCRIBE SECTION -->
     <section class="medium-padding-top section-image-bg-lime">
         <div class="container">
             <div class="row">
                 <div class="col-lg-7 col-md-12 mb-4">
                     <h4 class="subscribe-title ">Subscribe to our Newsletter</h4>
                     <p class="subscribe-subtitle text-white">
                         <span class="font-weight-bold">Join Our Newsletter</span> & Marketing Communication. We'll send you news and offers.
                     </p>
                     <form class="subscribe-form ">
                         <div class="input-btn--inline">
                             <input class="input--white" type="email" placeholder="Your email address">
                             <button type="button" class="crumina-button button--dark button--l">SUBSCRIBE</button>
                         </div>
                     </form>
                 </div>

                 <div class="col-lg-4 d-none d-lg-block mt-auto">
                     <img loading="lazy" src="img/theme-content/icons/08-subscribe.svg" alt="subscibe">
                 </div>
             </div>
         </div>
     </section>
     <!-- /SUBSCRIBE SECTION -->

     <!-- BACK TO TOP -->
     <div class="back-to-top">
         <svg class="crumina-icon">
             <use xlink:href="#icon-back-to-top"></use>
         </svg>
     </div>
     <!-- /BACK TO TOP -->

 </div>
 <!-- /MAIN CONTENT WRAPPER -->


 @endsection