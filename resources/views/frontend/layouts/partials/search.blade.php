 <!-- SEARCH POPUP -->
 <div class="modal fade window-popup popup-search-popup" id="popup-search" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">

             <div class="modal-body">
                 <div class="container">
                     <div class="row">
                         <form class="search-popup-form">
                             <input class="search-popup-input" placeholder="What are you looking for?" type="text" autofocus>
                             <button type="submit" class="search-popup-enter">
                                 <svg class="crumina-icon" width="35" height="35">
                                     <use xlink:href="#icon-enter"></use>
                                 </svg>
                             </button>
                             <div class="search-popup-close close" data-dismiss="modal">
                                 <svg class="crumina-icon" width="20" height="20">
                                     <use xlink:href="#icon-close"></use>
                                 </svg>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!-- /SEARCH POPUP -->