  <!-- MAIN HEADER -->
  <nav id="site-header" class="site-header navigation navigation-justified sticky-top">

      <div class="top-bar top-bar-dark">
          <div class="container">

              <div class="top-bar-content">
                  <div class="top-bar-item">

                      Nepal (<i class="fas fa-phone"></i>9813540375)
                  </div>

                  <div class="top-bar-item">
                      USA (903-884-6034)
                  </div>

                  <div class="top-bar-item">
                      <a href="#"><i class="fas fa-envelope">contact@travigoedu.com</i></a>
                  </div>

                  <div class="top-bar-item">
                      <span>Mon. - Fri.</span> 10:00 - 21:00
                  </div>

                  <div class="top-bar-item follow_us">
                      <span>Follow us:</span>
                      <div class="socials">
                          <a class="social-item" href="https://www.facebook.com/travigoedu">
                              <img loading="lazy" width="16" height="16" class="crumina-icon" src="{{url('frontend/img/theme-content/social-icons/facebook.svg')}}" alt="facebook">
                          </a>
                          <a class="social-item" href="https://www.youtube.com/neptubers">
                              <img loading="lazy" width="16" height="16" class="crumina-icon" src="{{url('frontend/img/theme-content/social-icons/youtube.svg')}}" alt="twitter">
                          </a>
                          <a class="social-item" href="https://insta.com/travigoedu">
                              <img loading="lazy" width="16" height="16" class="crumina-icon" src="{{url('frontend/img/theme-content/social-icons/instagram.svg')}}" alt="google">
                          </a>
                      </div>
                  </div>

                  <div class="top-bar-item login-block">
                      <svg class="crumina-icon" width="20" height="16">
                          <use xlink:href="#icon-users"></use>
                      </svg>
                      <a class="js-window-popup" href="{{route('login')}}">SIGN UP</a>
                  </div>
              </div>

              <a href="#" class="top-bar-close" id="top-bar-close-js">
                  <span></span>
                  <span></span>
              </a>

          </div>
      </div>

      <!-- MAIN HEADER CONTAINER -->
      <div class="container">

          <!-- MAIN HEADER RESPONSIVE -->
          <div class="navigation-header">

              <!-- MAIN HEADER RESPONSIVE LOGO -->
              <div class="navigation-logo">

                  <!-- MAIN HEADER RESPONSIVE LOGO LINK-->
                  <a class="site-logo" href="{{url('/')}}">

                      <!-- MAIN HEADER RESPONSIVE LOGO IMAGE-->
                      <img loading="lazy" src="{{url('logo.png')}}" alt="logo" width="70">
                      <!-- /MAIN HEADER RESPONSIVE LOGO IMAGE-->

       

                  </a>
                  <!-- /MAIN HEADER RESPONSIVE LOGO LINK-->

              </div>
              <!-- /MAIN HEADER RESPONSIVE LOGO -->

              <!-- TOP BAR RESPONSIVE BUTTON-OPEN -->
              <div id="top-bar-js" class="top-bar-link">
                  <svg class="crumina-icon" width="20" height="16">
                      <use xlink:href="#icon-users"></use>
                  </svg>
              </div>
              <!-- /TOP BAR RESPONSIVE BUTTON-OPEN -->

              <!-- MAIN HEADER RESPONSIVE BUTTON-OPEN -->
              <div class="navigation-button-toggler">

                  <!-- MAIN HEADER RESPONSIVE BUTTON-OPEN ICON -->
                  <i class="hamburger-icon"></i>
                  <!-- /MAIN HEADER RESPONSIVE BUTTON-OPEN ICON -->

              </div>
              <!-- /MAIN HEADER RESPONSIVE BUTTON-OPEN -->

          </div>
          <!-- /MAIN HEADER RESPONSIVE -->

          <!-- MAIN HEADER BODY -->
          <div class="navigation-body">

              <!-- MAIN HEADER BODY HEADER -->
              <div class="navigation-body-header">

                  <!-- MAIN HEADER LOGO -->
                  <div class="navigation-logo">

                      <!-- MAIN HEADER LOGO LINK -->
                      <a class="site-logo" href="{{url('/')}}">

                          <!-- MAIN HEADER RESPONSIVE LOGO IMAGE-->
                          <img loading="lazy" src="{{url('logo.png')}}" alt="logo" width="120">
                          <!-- /MAIN HEADER RESPONSIVE LOGO IMAGE-->



                      </a>
                      <!-- /MAIN HEADER LOGO LINK -->

                  </div>
                  <!-- /MAIN HEADER LOGO -->

                  <!-- MAIN HEADER RESPONSIVE BUTTON-CLOSE ICON -->
                  <span class="navigation-body-close-button">&#10005;</span>
                  <!-- /MAIN HEADER RESPONSIVE BUTTON-CLOSE ICON -->

              </div>
              <!-- /MAIN HEADER BODY HEADER -->

              <!-- MAIN HEADER MENU -->
              <ul class="navigation-menu ml-auto">

                  <!-- MAIN HEADER MENU ITEM -->
                  <li class="navigation-item"><a class="navigation-link" href="{{url('/')}}">Home</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="02_about_us.html">Countries</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="02_about_us.html">Blog</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="02_about_us.html">Universities</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="02_about_us.html">E-books</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="{{url('aboutUs')}}">About Us</a></li>
                  <li class="navigation-item"><a class="navigation-link" href="{{url('contactUs')}}">Contact Us</a></li>
              </ul>
              <!-- /MAIN HEADER MENU -->

              <!-- MAIN HEADER ADDITIONAL MENU -->
              <div class="navigation-body-section navigation-additional-menu">

                  <!-- MAIN HEADER ADDITIONAL MENU BUTTON -->
                  <a href="{{url('login')}}" class="crumina-button button--primary button--xs">Login</a>
                  <!-- /MAIN HEADER ADDITIONAL MENU BUTTON -->




              </div>
              <!-- /MAIN HEADER ADDITIONAL MENU -->


          </div>
          <!-- MAIN HEADER BODY -->

      </div>
      <!-- /MAIN HEADER CONTAINER -->

 
  </nav>
  <!-- /MAIN HEADER -->