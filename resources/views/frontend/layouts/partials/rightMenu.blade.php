   <!-- RIGHT MENU -->
   <div class="modal fade window-popup right-menu-popup" id="right-menu" tabindex="-1" role="dialog" aria-hidden="true">
       <div class="modal-dialog" role="document">
           <div class="modal-content">

               <div class="modal-body">
                   <div class="right-menu">

                       <div class="user-menu-close" data-dismiss="modal">
                           <div class="user-menu-content">
                               <span></span>
                               <span></span>
                           </div>
                       </div>

                       <div class="widget w-info">

                           <a class="site-logo" href="index.html">

                               <!-- MAIN HEADER RESPONSIVE LOGO IMAGE-->
                               <img loading="lazy" src="{{url('frontend/img/demo-content/logo/logo-dark.svg')}}" alt="logo" width="70">
                               <!-- /MAIN HEADER RESPONSIVE LOGO IMAGE-->

                               <div class="logo-text">
                                   <div class="logo-title"><span class="weight-black">TOP</span>TEN</div>
                                   <div class="logo-sub-title">Cool HTML Template</div>
                               </div>

                           </a>

                           <p class="widget-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius est etiam processus dynamicus vestibulum enim.</p>

                       </div>

                       <div class="widget w-login">

                           <h4 class="widget-title">Sign In to Your Account</h4>
                           <form method="post">
                               <div class="form-item">
                                   <input placeholder="Username or Email" type="text">
                               </div>
                               <div class="form-item">
                                   <input placeholder="Password" type="password">
                               </div>
                               <div class="form-item">
                                   <div class="remember-wrap">
                                       <label class="crumina-module crumina-checkbox control--checkbox">Remember Me
                                           <input type="checkbox">
                                           <span class="control__indicator"></span>
                                       </label>

                                       <a href="#">Lost your password?</a>
                                   </div>
                               </div>
                               <div class="form-item">
                                   <button class="crumina-button button--dark button--l w-100">AUTHORIZE</button>
                               </div>
                           </form>

                       </div>

                       <div class="widget w-contacts">

                           <h4 class="widget-title">Get In Touch</h4>
                           <p class="contacts-text">Lorem ipsum dolor sit amet, duis metus ligula amet in purus, vitae donec vestibulum enim.</p>

                           <div class="contact-item">
                               <img loading="lazy" class="crumina-icon" src="{{url('frontend/img/demo-content/icons/icon1.png')}}" alt="phone">
                               <div class="content">
                                   <a href="#" class="title">8 800 567.890.11</a>
                                   <p class="sub-title">Mon-Fri 9am-6pm</p>
                               </div>
                           </div>

                           <div class="contact-item">
                               <img loading="lazy" class="crumina-icon" src="{{url('frontend/img/demo-content/icons/icon2.png')}}" alt="mail">
                               <div class="content">
                                   <a href="#" class="title">info@topten.com</a>
                                   <p class="sub-title">online support</p>
                               </div>
                           </div>

                           <div class="contact-item">
                               <img loading="lazy" class="crumina-icon" src="{{url('frontend/img/demo-content/icons/icon3.png')}}" alt="location">
                               <div class="content">
                                   <a href="#" class="title">Melbourne, Australia</a>
                                   <p class="sub-title">795 South Park Avenue</p>
                               </div>
                           </div>

                       </div>

                   </div>
               </div>
           </div>
       </div>
   </div>
   <!-- /RIGHT MENU -->