<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- bootstrap 4.3.1 -->
    <link rel="stylesheet" href="{{asset('frontend/css/vendors/Bootstrap/bootstrap.min.css')}}">

    <!-- site header styles -->
    <link href="{{asset('frontend/css/plugins/navigation.min.css')}}" rel="stylesheet">

    <!-- main styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/main.css')}}">

    <!-- theme font -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/theme-font.min.css')}}">
    @yield('styles')
    <?php $CustomProperties = 'frontend/js/ie11CustomProperties.js'; ?>
    <!-- IE11 Support -->
    <script>
        window.MSInputMethodContext && document.documentMode && document.write('<script src="{{asset($CustomProperties)}}"><\x2fscript>');
    </script>

    <!-- styles for RTL -->
    <!--<link rel="stylesheet" type="text/css" href="css/rtl.min.css">-->

    <title>Travigo | Home</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('frontend/img/theme-content/favicon/android-chrome-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('frontend/img/theme-content/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('frontend/img/theme-content/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('frontend/img/theme-content/favicon/site.webmanifest')}}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


</head>

<body>