 @extends('backend.layouts.app')

 @section('breadcrumb')
 Schools List
 @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Schools</li>
 @endsection
 @section('content')
 <!-- Container fluid  -->
 <!-- ============================================================== -->
 <div class="container-fluid">
     <!-- ============================================================== -->
     <!-- Start Page Content -->
     <!-- ============================================================== -->
     <!-- Row -->
     <div class="row">
         <div class="col-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-align-justify"></i>
                     Schools
                     <a class="float-right" href="{{ route('schools.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                 </div>
                 <div class="table-responsive">
                     <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th scope="col">#</th>
                                 <th scope="col">Name</th>
                                 <th scope="col">Country</th>
                                 <th scope="col">Address</th>

                                 <th scope="col">Courses</th>
                                 <th scope="col">Features</th>
                                 <th scope="col">Action</th>
                             </tr>
                         </thead>
                         <tbody>
                             @foreach($schools as $key => $school)
                             <tr>
                                 <th scope="row">{{++$key}}</th>
                                 <td>{{$school->name}}</td>
                                 <td>{{isset($school->schoolAddress->country)?$school->schoolAddress->country:''}}</td>
                                 <td>{{isset($school->schoolAddress->address)?$school->schoolAddress->address:''}}</td>



                                 <td>
                                     <div class='btn-group'>
                                         <a href="{{ route('schools.createCourses', [$school->id]) }}" class='btn btn-info'>Add Courses</a>
                                     </div>
                                 </td>
                                 <td>
                                     <div class='btn-group'>
                                         <a href="{{ route('schools.createFeatures', [$school->id]) }}" class='btn btn-warning ml-2'>Add Features</a>
                                     </div>
                                 </td>
                                 <td>
                                     {!! Form::open(['route' => ['schools.destroy', $school->id], 'method' => 'delete']) !!}
                                     <div class='btn-group'>
                                         <a href="{{ route('schools.createImages', [$school->id]) }}" class='btn btn-ghost-info'><i class="fa fa-image"></i></a>
                                         <a href="{{ route('schools.edit', [$school->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                         {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                     </div>
                                     {!! Form::close() !!}

                                 </td>
                             </tr>
                             @endforeach
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
         <!-- Column -->
     </div>
     <!-- Row -->
     <!-- ============================================================== -->
     <!-- End PAge Content -->
     <!-- ============================================================== -->
     <!-- ============================================================== -->
     <!-- Right sidebar -->
     <!-- ============================================================== -->
     <!-- .right-sidebar -->
     <!-- ============================================================== -->
     <!-- End Right sidebar -->
     <!-- ============================================================== -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 @endsection