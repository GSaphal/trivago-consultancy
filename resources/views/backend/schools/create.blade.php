 @extends('backend.layouts.app')

 @section('breadcrumb')
 Add School
 @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item"><a href="{{route('schools.index')}}" class="text-muted">Schools</a></li>
 <li class="breadcrumb-item text-muted active" aria-current="page">Create</li>
 @endsection
 @section('content')
 <!-- Container fluid  -->
 <!-- ============================================================== -->
 <div class="container-fluid">
     <!-- ============================================================== -->
     <!-- Start Page Content -->
     <!-- ============================================================== -->
     <!-- Row -->
     {!! Form::open(['route' => 'schools.store','class'=>'form-horizontal form-material','files' => true]) !!}

     <div class="row">
         <div class="col-lg-8 col-xlg-8col-md-8">
             <div class="card">
                 <div class="card-header">
                     General Information
                 </div>

                 <div class="card-body">

                     <div class="form-group">
                         <div class="row">

                             <div class="col-md-4">
                                 <label>School Name</label>
                                 {!! Form::text('name', null, ['class' => 'form-control form-control-line']) !!}
                             </div>

                             <div class="col-md-4">
                                 <label>School Type</label>
                                 {!! Form::text('school_type', null, ['class' => 'form-control form-control-line']) !!}
                             </div>
                             <div class="col-md-4">
                                 <label>Total Students</label>
                                 {!! Form::text('total_students', null, ['class' => 'form-control form-control-line']) !!}
                             </div>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">



                             <div class="col-md-8">
                                 <label>Description</label>
                                 {!! Form::textarea('description', null, ['class' => 'form-control form-control-line','rows'=>'4']) !!}
                             </div>
                             <div class="col-md-4">
                                 <label>Logo</label>
                                 {!! Form::file('logo', null, ['class' => 'form-control form-control-line']) !!}
                             </div>



                         </div>
                     </div>


                 </div>

             </div>
         </div>
         <div class="col-lg-4 col-xlg-4 col-md-4">
             <div class="card">
                 <div class="card-header">
                     School Finance
                 </div>
                 <div class="card-body">

                     <div class="form-group">


                         <div class="row">

                             <label>Tution Fee</label>
                             {!! Form::text('tution_fee', null, ['class' => 'form-control form-control-line']) !!}
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">

                             <label>Admission Fee</label>
                             {!! Form::text('admission_fee', null, ['class' => 'form-control form-control-line']) !!}
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">

                             <label>Annual Charge</label>
                             {!! Form::text('annual_charges', null, ['class' => 'form-control form-control-line']) !!}
                         </div>
                     </div>





                 </div>

             </div>
         </div>
     </div>
     <div class="row">
         <div class="col-lg-12 col-xlg-12 col-md-12">
             <div class="card">
                 <div class="card-header">
                     School Location
                 </div>


                 <div class="card-body">

                     <div class="form-group">
                         <div class="row">

                             <div class="col-md-6">
                                 <label>Country</label>
                                 {!! Form::text('country', null, ['class' => 'form-control form-control-line']) !!}
                             </div>

                             <div class="col-md-6">
                                 <label>Province</label>
                                 {!! Form::text('province', null, ['class' => 'form-control form-control-line']) !!}
                             </div>

                         </div>
                     </div>
                     <div class="form-group">
                         <div class="row">


                             <div class="col-md-4">
                                 <label>City</label>
                                 {!! Form::text('city', null, ['class' => 'form-control form-control-line']) !!}
                             </div>
                             <div class="col-md-4">
                                 <label>Address</label>
                                 {!! Form::text('address', null, ['class' => 'form-control form-control-line']) !!}
                             </div>
                             <div class="col-md-4">
                                 <label>Zip Code</label>
                                 {!! Form::text('zip_code', null, ['class' => 'form-control form-control-line']) !!}
                             </div>


                         </div>
                     </div>


                 </div>

             </div>
         </div>
     </div>
     <div class="form-group">
         <div class="col-sm-12">
             <button class="btn btn-success">Save</button>
         </div>
     </div>
     {!! Form::close() !!}
     <!-- Row -->
     <!-- ============================================================== -->
     <!-- End PAge Content -->
     <!-- ============================================================== -->
     <!-- ============================================================== -->
     <!-- Right sidebar -->
     <!-- ============================================================== -->
     <!-- .right-sidebar -->
     <!-- ============================================================== -->
     <!-- End Right sidebar -->
     <!-- ============================================================== -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 @endsection