@extends('backend.layouts.app')
@section('styles')
<style>
    input[type="file"] {
        display: block;
    }

    .imageThumb {
        max-height: 75px;
        border: 2px solid;
        padding: 1px;
        cursor: pointer;
    }

    .pip {
        display: inline-block;
        margin: 10px 10px 0 0;
    }

    .img-delete {
        display: block;
        background: #444;
        border: 1px solid black;
        color: white;
        text-align: center;
        cursor: pointer;
    }

    .img-delete:hover {
        background: white;
        color: black;
    }
</style>
@endsection
@section('breadcrumb')
Upload Images
@endsection
@section('breadcrumb-item')
<li class="breadcrumb-item"><a href="{{route('schools.index')}}" class="text-muted">Schools</a></li>
<li class="breadcrumb-item text-muted active" aria-current="page">Images</li>
@endsection
@section('content')

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12 col-xlg-12 col-md-12">

            <div class="card mt-3">
                <!-- Tabs -->

                <!-- Tabs -->

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12 mb-50">
                            <form method="post" action="{{route('schools.images.upload')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                                @csrf
                                <input type="hidden" name="school_id" value="{{$school->id}}">
                            </form>
                        </div>
                    </div>
                    <?php

                    $schoolImages = App\Models\SchoolImage::where('school_id', $school->id)->get(); ?>

                    <div class="row mt-4">
                        @if(isset($schoolImages))
                        @foreach($schoolImages as $data)

                        <div class=" col-md-3">
                            <div style="width:200px;height:200px;background:url('{{url('storage/schools/'.$data->image)}}');background-size:cover;background-repeat:no-repeat;background-position:center;"></div>
                            <!-- <img class="image" src="{{url('storage/schools/'.$data->image)}}"> -->
                            <a class="button" href="{{route('schools.images.destroy',['data'=>$data->image])}}">
                                <i class="fa fa-times-circle fa-2x"></i>
                            </a>
                        </div>
                        @endforeach
                        @endif
                    </div>

                </div>

            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
@endsection
@section('scripts')
<script>
    Dropzone.prototype.defaultOptions.dictDefaultMessage = "Add at least 4 photos of your school.Click here to upload.";
    Dropzone.options.dropzone = {
        maxFilesize: 12,
        maxFiles: 6,
        renameFile: function(file) {
            var dt = new Date();
            var time = dt.getTime();
            return time + file.name;
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 5000,
        removedfile: function(file) {
            var name = file.upload.filename;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'POST',
                url: '{{ url("upload/delete") }}',
                data: {
                    filename: name
                },
                success: function(data) {
                    console.log("File has been successfully removed!!");
                },
                error: function(e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {},
        error: function(file, response) {
            return false;
        }
    };
</script>
@endsection