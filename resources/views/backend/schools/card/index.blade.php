 @extends('backend.layouts.app')
 @section('styles')
 <style>
     html {
         font-family: Lato, 'Helvetica Neue', Arial, Helvetica, sans-serif;
         font-size: 14px;
     }

     h5 {
         font-size: 1.28571429em;
         font-weight: 700;
         line-height: 1.2857em;
         margin: 0;
     }

     .card {
         font-size: 1em;
         overflow: hidden;
         padding: 0;
         border: none;
         border-radius: .28571429rem;
         box-shadow: 0 1px 3px 0 #d4d4d5, 0 0 0 1px #d4d4d5;
     }

     .card-block {
         font-size: 1em;
         position: relative;
         margin: 0;
         padding: 1em;
         border: none;
         border-top: 1px solid rgba(34, 36, 38, .1);
         box-shadow: none;
     }

     .card-img-top {
         display: block;
         width: 100%;
         height: auto;
     }

     .card-title {
         font-size: 1.28571429em;
         font-weight: 700;
         line-height: 1.2857em;
     }

     .card-text {
         clear: both;
         margin-top: .5em;
         color: rgba(0, 0, 0, .68);
     }

     .card-footer {
         font-size: 1em;
         position: static;
         top: 0;
         left: 0;
         max-width: 100%;
         padding: .75em 1em;
         color: rgba(0, 0, 0, .4);
         border-top: 1px solid rgba(0, 0, 0, .05) !important;
         background: #fff;
     }

     .card-inverse .btn {
         border: 1px solid rgba(0, 0, 0, .05);
     }

     .profile {
         position: absolute;
         top: -12px;
         display: inline-block;
         overflow: hidden;
         box-sizing: border-box;
         width: 25px;
         height: 25px;
         margin: 0;
         border: 1px solid #fff;
         border-radius: 50%;
     }

     .profile-avatar {
         display: block;
         width: 100%;
         height: 100%;
         border-radius: 50%;
     }

     .profile-inline {
         position: relative;
         top: 0;
         display: inline-block;
     }

     .profile-inline~.card-title {
         display: inline-block;
         margin-left: 4px;
         vertical-align: top;
     }

     .text-bold {
         font-weight: 700;
     }

     .meta {
         font-size: 1em;
         color: rgba(0, 0, 0, .4);
     }

     .meta a {
         text-decoration: none;
         color: rgba(0, 0, 0, .4);
     }

     .meta a:hover {
         color: rgba(0, 0, 0, .87);
     }
 </style>
 @endsection
 @section('breadcrumb')
 Find Schools
 @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Schools</li>
 @endsection
 @section('content')
 <div class="container-fluid">
     <div class="row p-0">
            <div class=" col-md-12 col-lg-12 pb-5 ">
            <form method="get" action="{{route('schools.findSchools')}}">
                <div class="card-body row no-gutters align-items-center p-0">
                    <div class="col">
                        <input name="schoolName" class="form-control form-control-lg form-control-borderless" type="search" placeholder="Search for any school" value="{{isset($schoolName)?$schoolName:''}}">
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <button class="btn btn-lg btn-primary" type="submit">Search</button>
                    </div>
                    <!--end of col-->
                </div>
            </form>
        </div>
    </div>
     <div class="row">
         <!-- Column -->
         <div class="col-lg-3 col-xlg-3 col-md-5">
             <div class="card">
                 <div class="card-body">
                     <div class="row">
                         <div class="col-12 align-self-center">
                         <h3>Select any Options</h3>
                         <hr/>

                             <div class="customize-input">
                                 <label>Courses</label>
                                 <select class="custom-select"  name="course">
                                    <option value="" selected disabled>Select a Course</option>
                                 @foreach($courses as $course) 
                                 {{$course}}
                                 {{$course->course_name}}   
                                     <option value="{{$course->course_name}}">{{$course->course_name}}</option>
                                   @endforeach  
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                             <div class="customize-input">
                                 <label>Country</label>

                                 <select class="custom-select"  name="country">
                                 <option value="" selected disabled>Select a Country</option>


                                     <option value="Nepal">Nepal</option>
                                     <option value="Canada">Canada</option>
                                     <option value="USA">USA</option>

                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                             <div class="customize-input">
                                 <label>Address</label>
                                 <select class="custom-select">
                                     <option selected>June 19</option>
                                     <option value="1">July 19</option>
                                     <option value="2">Aug 19</option>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                             <div class="customize-input">
                                 <label>School Type</label>
                                 <select class="custom-select form-control-line">
                                 <option value="" selected disabled>Select a School Type</option>

                                     <option value="University">University</option>
                                     <option value="College">College</option>
                                     <option value="High School">High School</option>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                         <label>Application Fee</label>
                             <div class="customize-input">
                                <div class="selector">
                                    <div class="price-slider">
                                        <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                                            <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                            <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                        </div>
                                        <span id="min-price" data-currency="Rs" class="slider-price">0</span><span id="max-price" data-currency="Rs" data-max="100000"  class="slider-price">100000</span>
                                    </div> 
                                </div>       
                             </div>
                         </div>
                     </div>
                  
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                             <div class="customize-input">
                                 <label>Intakes</label>
                                 <select class="custom-select">
                                     <option selected>June 19</option>
                                     <option value="1">July 19</option>
                                     <option value="2">Aug 19</option>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <div class="row mt-3">
                         <div class="col-12 align-self-center">
                             <div class="customize-input">
                                <button class="btn btn-lg btn-primary">Filter</button>
                             </div>
                         </div>
                     </div>
                 </div>

             </div>
         </div>
         <!-- Column -->
         <!-- Column -->
         <div class="col-lg-8 col-xlg-8 col-md-7">

             <div class="row">
                 @foreach($schools as $key => $school)
                 <div class="col-sm-6 col-md-3 col-lg-3">
                     <div class="card card-inverse card-info">
                         <img class="card-img-top" src="{{url('storage/'.$school->logo)}}">
                         <div class="card-block">
                      
                            <a href="{{route('schools.show',[$school->id])}}"> <h4 class="card-title">{{$school->name}}</h4></a>
                             <div class="card-text">
                                 {{isset($school->schoolAddress->address)?$school->schoolAddress->address:''}}, {{isset($school->schoolAddress->city)?$school->schoolAddress->city:''}}, {{isset($school->schoolAddress->country)?$school->schoolAddress->country:''}}
                             </div>
                         </div>
                         <div class="card-footer">
                         <a href="{{route('schools.show',[$school->id])}}">    <button class="btn btn-info btn-md">View Details</button></a>
                         </div>
                     </div>
                 </div>
                 @endforeach
             </div>

         </div>
         <!-- Column -->
     </div>
     <!-- Row -->
     <!-- ============================================================== -->
     <!-- End PAge Content -->
     <!-- ============================================================== -->
     <!-- ============================================================== -->
     <!-- Right sidebar -->
     <!-- ============================================================== -->
     <!-- .right-sidebar -->
     <!-- ============================================================== -->
     <!-- End Right sidebar -->
     <!-- ============================================================== -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 @endsection