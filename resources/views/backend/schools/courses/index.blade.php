@extends('backend.layouts.app')

@section('breadcrumb')
Add Courses
@endsection
@section('breadcrumb-item')
<li class="breadcrumb-item"><a href="{{route('schools.index')}}" class="text-muted">Schools</a></li>
<li class="breadcrumb-item text-muted active" aria-current="page">Courses</li>
@endsection
@section('content')

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12 col-xlg-12 col-md-12">

            <div class="card mt-3">
                <!-- Tabs -->

                <!-- Tabs -->

                <div class="card-body">
                    {!! Form::open(['route' => 'schools.storeCourses','class'=>'form-horizontal form-material']) !!}
                    {!! Form::hidden('school_id', isset($school)?$school->id:null, ['class' => 'form-control']) !!}

                    @if(count( $schoolCourses)!=0)
                    @foreach($schoolCourses as $key=> $schoolCourse)
                    <div class="multi-field-wrapper">
                        <div class="multi-fields">
                            <div class="multi-field">
                                <div class="form-group">
                                    <div class="row">
                                        {!! Form::hidden('id[]', isset($schoolCourse)?$schoolCourse->id:null, ['class' => 'form-control']) !!}
                                        <div class="col-md-4">
                                            <label>Course Name</label>
                                            {!! Form::text('course_name[]', isset($schoolCourse)?$schoolCourse->course_name:null, ['class' => 'form-control form-control-line']) !!}
                                        </div>

                                        <div class="col-md-3">
                                            <label>Start Date</label>
                                            {!! Form::date('start_date[]', isset($schoolCourse)?$schoolCourse->start_date:null, ['class' => 'form-control form-control-line']) !!}
                                        </div>
                                        <div class="col-md-3">
                                            <label>Application Fee</label>
                                            {!! Form::text('application_fee[]', isset($schoolCourse)?$schoolCourse->application_fee:null, ['class' => 'form-control form-control-line']) !!}
                                        </div>
                                        <div class="col-md-2 remove-btn mt-4">
                                            <button type="button" class="remove-field btn btn-danger">x</button>
                                        </div>
                                    </div>
                                </div>






                            </div>
                        </div>
                        @if(count($schoolCourses)==$key+1)
                        <button type="button" class="add-field btn btn-primary">ADD</button>
                        @endif

                    </div>
                    @endforeach
                    @else
                    <div class="multi-field-wrapper">
                        <div class="multi-fields">
                            <div class="multi-field">
                                <div class="form-group">
                                    <div class="row">
                                        {!! Form::hidden('id[]', isset($schoolCourse)?$schoolCourse->id:null, ['class' => 'form-control']) !!}
                                        <div class="col-md-4">
                                            <label>Course Name</label>
                                            {!! Form::text('course_name[]', null, ['class' => 'form-control form-control-line']) !!}
                                        </div>

                                        <div class="col-md-3">
                                            <label>Start Date</label>
                                            {!! Form::date('start_date[]',null, ['class' => 'form-control form-control-line']) !!}
                                        </div>
                                        <div class="col-md-3">
                                            <label>Application Fee</label>
                                            {!! Form::text('application_fee[]', null, ['class' => 'form-control form-control-line']) !!}
                                        </div>
                                        <div class="col-md-2 remove-btn mt-4">
                                            <button type="button" class="remove-field btn btn-danger">x</button>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>

                        <button type="button" class="add-field btn btn-primary">ADD</button>


                    </div>
                    @endif
                    <div class="form-group mt-4 float-right">
                        <div class="col-sm-12">
                            <button class="btn btn-success">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
@endsection
@section('scripts')
<script>
    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        });
        $('.multi-field .remove-field', $wrapper).click(function() {


            $(this).parent('.remove-btn').parent('.row').parent('.form-group').parent('.multi-field').remove();
        });
    });
</script>
@endsection