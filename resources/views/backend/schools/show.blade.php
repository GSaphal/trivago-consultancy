@extends('backend.layouts.app')

@section('breadcrumb')
Schools List
@endsection
@section('breadcrumb-item')
<li class="breadcrumb-item text-muted active" aria-current="page">Schools / {{$school->name}}</li>
@endsection
@section('content')


<div class="container-fluid">
        
                <div class="row">
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                        
                                <center class="mt-4"> <img src="{{url('storage/'.$school->logo)}}"
                                        width="300" class="img-fluid" />
                                    <h4 class="card-title mt-2">{{$school->name}}</h4>
                                    <h6 class="card-subtitle">Type: {{$school->school_type}}</h6>
                                    <div class="row text-center justify-content-md-center mt-3">
                                  
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Full Name</strong>
                                                <br>
                                                <p class="text-muted">Johnathan Deo</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted">(123) 456 7890</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">johnathan@admin.com</p>
                                            </div>
                                            <div class="col-md-6 col-xs-6"> <strong>Location</strong>
                                                <br>
                                                <p class="text-muted">London</p>
                                            </div>
                                    </div>
                                </center>
                            </div>
                            <div>
                                <hr>
                            </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>hannagover@gmail.com</h6> <small class="text-muted pt-4 db">Phone</small>
                                <h6>+91 654 784 547</h6> <small class="text-muted pt-4 db">Address</small>
                              
                                <h6>{{$school->schoolAddress->address}}, {{$school->schoolAddress->city}}, {{$school->schoolAddress->country}}</h6>
                                <div class="map-box">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508"
                                        width="100%" height="250px" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                                </div> <small class="text-muted pt-4 db">Social Profile</small>
                                <br />
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-facebook-f"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-youtube"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Tabs -->
                            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-timeline-tab" data-toggle="pill"
                                        href="#current-month" role="tab" aria-controls="pills-timeline"
                                        aria-selected="true">Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month"
                                        role="tab" aria-controls="pills-profile" aria-selected="false">Courses Available</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                                        role="tab" aria-controls="pills-setting" aria-selected="false">Fee Structure</a>
                                </li>
                            </ul>
                            <!-- Tabs -->
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="current-month" role="tabpanel"
                                    aria-labelledby="pills-timeline-tab">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p >{{$school->description}}</p>

                                            </div>
                                            <div class="col-md-6">
                                                    <div class="row">
                                                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                                                                        <ol class="carousel-indicators">
                                                                        @foreach($school->schoolImages as $schoolImage)

                                                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="<?php if($loop->index==0){echo("active");}?>"></li>
                                                                        @endforeach

                                                                        </ol>
                                                                        <div class="carousel-inner">
                                                                        @foreach($school->schoolImages as $schoolImage)
                                                                            {{$loop->index}}
                                                                            <div class="<?php if($loop->index==0){echo("carousel-item active");}else{echo("carousel-item");}?>">
                                                                            <img class="d-block w-100" src="{{url('storage/schools/'.$schoolImage->image)}}" alt="First slide" height="400">
                                                                            </div>
                                                                            @endforeach

                                                                        </div>
                                                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Previous</span>
                                                                        </a>
                                                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                            <span class="sr-only">Next</span>
                                                                        </a>
                                                                </div>


                                                        </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <h4 class="font-weight-medium mt-4">Features</h4>
                                                 <hr/>
                                            </div>   
                                            @foreach($school->schoolFeatures as $schoolFeature)
                                            <div class="col-md-4 col-sm-4">
                                                    <div class="feature">
                                                        <div class="feature-icon">
                                                        <div class="mdi mdi-account-convert text-center" style="font-size:60px;"></div>
                                                        </div>
                                                        <h5 class="text-center">{{$schoolFeature->name}}</h5>
                                                        <p class="text-center">
                                                           {{$schoolFeature->description}}
                                                        </p>
                                                    </div>
                                            </div>
                                            @endforeach
                                               

                                                
                                        </div>

                                            
                                        
                                           
                                
                                    </div>
                                  
                                </div>
                        
                              
                                <div class="tab-pane fade" id="last-month" role="tabpanel"
                                    aria-labelledby="pills-profile-tab">
                                    <div class="card-body">
                                    <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                        <th scope="col" style="width:5%">S.N</th>

                                            <th scope="col" style="width:35%">Course Name</th>
                                            <th scope="col" style="width:20%">Enrollment Date</th>
                                            <th scope="col" style="width:20%">Application Fee</th>
                                            <th scope="col" style="width:20%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=0?>
                                        @foreach($school->schoolCourse as $schoolCourse)
                                        <tr>
                                                <td>{{++$count}}</td>
                                                <td scope="row">{{$schoolCourse->course_name}}</td>
                                                <td><div class="mdi mdi-calendar-clock"><span class="pl-2">{{$schoolCourse->start_date}}</span></div></td>
                                                <td>Rs. {{$schoolCourse->application_fee}}</td>
                                                <td><button class="btn btn-primary">Apply now</button></td>
                                      
                                        </tr>
                                        @endforeach
                                
                                    </tbody>
                                </table>
                            </div>
                                       
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="previous-month" role="tabpanel"
                                    aria-labelledby="pills-setting-tab">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Johnathan Doe"
                                                        class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="johnathan@admin.com"
                                                        class="form-control form-control-line" name="example-email"
                                                        id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="password"
                                                        class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="123 456 7890"
                                                        class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Message</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5"
                                                        class="form-control form-control-line"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">Select Country</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-line">
                                                        <option>London</option>
                                                        <option>India</option>
                                                        <option>Usa</option>
                                                        <option>Canada</option>
                                                        <option>Thailand</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         
            </div>

@endsection


