 @extends('backend.layouts.app')
 @section('breadcrumb') Users @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Users</li>
 @endsection
 @section('content')
 <div class="container-fluid">

     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-align-justify"></i>
                     Users
                     <a class="float-right" href="{!! route('users.create') !!}"><i class="fa fa-plus-square fa-lg"></i></a>
                 </div>
                 <div class="card-body">
                     @include('backend.users.table')
                     <div class="pull-right mr-3">



                     </div>
                 </div>
             </div>
         </div>
     </div>

 </div>
 @endsection
 @section('scripts')
 <script>
     $(document).ready(function() {
         $('#users-table').DataTable();
     });
 </script>

 @endsection