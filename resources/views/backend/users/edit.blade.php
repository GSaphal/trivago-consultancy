 @extends('backend.layouts.app')
 @section('breadcrumb') Edit User @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item"><a href="{{route('users.index')}}" class="text-muted">Users</a></li>
 <li class="breadcrumb-item text-muted active" aria-current="page">Edit</li>
 @endsection
 @section('content')

 <div class="container-fluid">

     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-edit fa-lg"></i>
                     <strong>Edit Role</strong>
                 </div>
                 <div class="card-body">
                     {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                     @include('backend.users.fields')

                     {!! Form::close() !!}
                 </div>
             </div>
         </div>
     </div>

 </div>
 @endsection