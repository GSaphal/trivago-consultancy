 <!-- All Jquery -->
 <!-- ============================================================== -->
 <script src="{{asset('backend/assets/libs/jquery/dist/jquery.min.js')}}"></script>
 <script src="{{asset('backend/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
 <script src="{{asset('backend/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
 <!-- apps -->
 @yield('scripts')
 <script src="{{asset('backend/dist/js/app-style-switcher.js')}}"></script>
 <script src="{{asset('backend/dist/js/feather.min.js')}}"></script>
 <!-- slimscrollbar scrollbar JavaScript -->
 <script src="{{asset('backend/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
 <!--Menu sidebar -->
 <script src="{{asset('backend/dist/js/sidebarmenu.js')}}"></script>
 <!--Custom JavaScript -->
 <script src="{{asset('backend/dist/js/custom.min.js')}}"></script>
 

 <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"></link>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js "></script>

<script>
    $("#slider-range").slider({
  range: true, 
  min: 0,
  max: 10000000,
  step: 50,
  slide: function( event, ui ) {
    $( "#min-price").html(ui.values[ 0 ]);
    
    suffix = '';
    if (ui.values[ 1 ] == $( "#max-price").data('max') ){
       suffix = ' +';
    }
    $( "#max-price").html(ui.values[ 1 ] + suffix);         
  }
})

    </script>
 </body>

 </html>