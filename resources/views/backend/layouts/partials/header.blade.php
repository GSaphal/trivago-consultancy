<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 4 admin, bootstrap 4, css3 dashboard, bootstrap 4 dashboard, severny admin bootstrap 4 dashboard, frontend, responsive bootstrap 4 admin template, severny design, severny dashboard bootstrap 4 dashboard template">
    <meta name="description" content="Severny is powerful and clean admin dashboard template, inpired from Bootstrap Framework">
    <meta name="robots" content="noindex,nofollow">
    <title>Trivago Consultancy</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <!-- Custom CSS -->
    <link href="{{asset('backend/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('backend/dist/css/style.min.css')}}" rel="stylesheet">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    
    <style>
        .form-control {
            padding: 20px 15px !important;
        }

        .nav {
            padding: 20px;
        }

        .nav-pills.custom-pills .nav-link {
            color: black;
            font-weight: 400;

        }

        .nav-pills.custom-pills .nav-link.active {
            background: #544EFA;
            color: white;
            border-radius: 10px;

        }
        .remove-field {
            margin-top:5px;
        }
        .selector {
    position: relative;
    padding: 5px;
    color: #7e7e7e;
}

.selector ul {
    position: relative;
    display: block;
    overflow: auto;
    min-width: 138px;
    max-height: 200px;
    background: #fff;
    list-style: none;
    white-space: inherit;
    padding-right: 17px;
    width: calc(100% + 17px)
}

.selector li {
    position: relative;
    padding: 3px 20px 3px 25px;
    cursor: pointer
}

.selector li:before {
    position: absolute;
    top: 50%;
    left: 0;
    top: 4px;
    display: inline-block;
    margin-right: 9px;
    width: 17px;
    height: 17px;
    background-color: #f4f4f4;
    border: 1px solid #d5d5d5;
    content: ""
}

.selector li[data-selected="1"]:before {
    border: 1px solid #d7d7d7;
    background-color: #fff
}

.selector li[data-selected="1"]:after {
    position: absolute;
    top: 50%;
    left: 3px;
    top: 11px;
    display: inline-block;
    width: 4px;
    height: 10px;
    border-right: 2px solid;
    border-bottom: 2px solid;
    background: none;
    color: #4450F7;
    content: "";
    -webkit-transform: rotate(40deg) translateY(-50%);
    transform: rotate(40deg) translateY(-50%)
}

.selector li:hover {
    color: #aaa
}

.selector li .total {
    position: absolute;
    right: 0;
    color: #d7d7d7
}

.selector .price-slider {
    text-align: center;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    position: relative;
    padding-top: 10px
}

@media (min-width: 768px) {
    .selector .price-slider {
        padding-top:8px
    }
}

.selector .price-slider:before {
    position: absolute;
    top: 50%;
    left: 0;
    margin-top: 0;
    color: #4450F7;
    content: attr(data-currency);
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%)
}

.selector #slider-range {
    width: 90%;
    margin-bottom: 10px;
    border: none;
    background: #e2f7f2;
    height: 3px;
    margin-left: 8px;
    margin-right: 8px
}

@media (min-width: 768px) {
     .selector #slider-range {
        width:100%
    }
}

.selector .ui-slider-handle {
    border-radius: 50%;
    background-color: #4450F7;
    border: none;
    top: -14px;
    width: 28px;
    height: 28px;
    outline: none
}

@media (min-width: 768px) {
    .selector .ui-slider-handle {
        top:-7px;
        width: 16px;
        height: 16px
    }
}

.selector .ui-slider-range {
    background-color: #d7d7d7
}

.selector .slider-price {
    position: relative;
    padding: 5px 40px;
    line-height: 20px;
    text-align: center
}

.selector .slider-price:before {
    position: absolute;
    top: 50%;
    left: 13px;
    margin-top: 0;
    color: #4450F7;
    content: attr(data-currency);
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%)
}

.selector .show-all {
    position: relative;
    padding-left: 25px;
    color: #4450F7;
    cursor: pointer;
    line-height: 28px
}

.selector .show-all:after, .selector .show-all:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 4px;
    margin-top: -1px;
    color: #4450F7;
    width: 10px;
    border-bottom: 1px solid
}

.selector .show-all:after {
    -webkit-transform: rotate(90deg);
    transform: rotate(90deg)
}

.selector.open ul {
    max-height: none
}

.selector.open .show-all:after {
    display: none
}


* {
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
}
    </style>
    @yield('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>