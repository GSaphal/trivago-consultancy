<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin6">
    <div class="user-profile text-center pt-2">
        <div class="d-flex align-items-center justify-content-center pb-3">
            <div class="dropdown sub-dropdown">
                <button class="btn profile-pic rounded-circle position-relative" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="badge rounded-circle badge-success profile-dd text-center"><i
                            class="fas fa-angle-down"></i></span>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="fas fa-circle text-success font-12 mr-2"></i>
                        Active
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="fas fa-circle text-warning font-12 mr-2"></i>
                        Away
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="fas fa-circle text-danger font-12 mr-2"></i>
                        Do not Disturb
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="fas fa-circle text-muted font-12 mr-2"></i>
                        Invisible
                    </a>
                </div>
            </div>
        </div>
        <div class="profile-section">
            <p class="font-weight-light mb-0 font-18">{{Auth::user()->name}}</p>
            <span class="op-7 font-14">Marketing Head</span>
            <div class="row border-top border-bottom mt-3 no-gutters">
                <div class="col-4 border-right">
                    <a class="p-3 d-block menubar-height" href="javascript:void(0)" id="bell" data-display="static"
                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span><i data-feather="bell" class="svg-icon op-7"></i></span>
                        <span class="badge badge-danger badge-no rounded-circle">5</span>
                    </a>
                </div>
                <div class="col-4 border-right">
                    <a class="p-3 d-block menubar-height" id="bottom-sidebar" href="javascript:void(0)" role="button">
                        <span><i data-feather="settings" class="svg-icon op-7"></i></span>
                    </a>
                </div>
                <div class="col-4">
                    <a class="p-3 d-block menubar-height" href="javascript:void(0)" role="button" data-display="static"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span><i data-feather="message-square" class="svg-icon op-7"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap"><span class="hide-menu"></span></li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{url('home')}}" aria-expanded="false">
                        <i data-feather="home" class="feather-icon"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('students.index')}}" aria-expanded="false">
                        <i data-feather="users" class="feather-icon"></i>
                        <span class="hide-menu">Profile</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('schools.index')}}" aria-expanded="false">
                        <i data-feather="grid" class="feather-icon"></i>
                        <span class="hide-menu">School</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('students.schoolIndex')}}" aria-expanded="false">
                        <i data-feather="anchor" class="feather-icon"></i>
                        <span class="hide-menu">Student School</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('permissions.index')}}" aria-expanded="false">
                        <i data-feather="feather" class="feather-icon"></i>
                        <span class="hide-menu">Permissions</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('roles.index')}}" aria-expanded="false">
                        <i data-feather="check-square" class="feather-icon"></i>
                        <span class="hide-menu">Roles</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('users.index')}}" aria-expanded="false">
                        <i data-feather="user-x" class="feather-icon"></i>
                        <span class="hide-menu">Users</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link sidebar-link" href="{{route('schools.findSchools')}}" aria-expanded="false">
                        <i data-feather="user-x" class="feather-icon"></i>
                        <span class="hide-menu">Find Schools</span>
                    </a>
                </li>
      
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->