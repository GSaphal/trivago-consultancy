  @include('backend.layouts.partials.header')
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
      <div class="lds-ripple">
          <div class="lds-pos"></div>
          <div class="lds-pos"></div>
      </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin1" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="relative" data-boxed-layout="full">
      <div class="app-container" data-navbarbg="skin1"></div>


      @include('backend.layouts.partials.navbar')
      @include('backend.layouts.partials.sidebar')

      <!-- Page wrapper  -->

      <div class="page-wrapper">
          <div class="page-breadcrumb">
              <div class="row">

                  <div class="col-8 align-self-center">
                      @include('flash::message')
                  </div>
              </div>
              <div class="row">

                  <div class="col-5 align-self-center">
                      <h4 class="page-title text-truncate text-dark font-weight-medium"> @yield('breadcrumb')</h4>
                      <div class="d-flex align-items-center">
                          <nav aria-label="breadcrumb">
                              <ol class="breadcrumb m-0 p-0">
                                  <li class="breadcrumb-item"><a href="{{url('/home')}}" class="text-muted">Home</a></li>
                                  @yield('breadcrumb-item')
                              </ol>
                          </nav>
                      </div>
                  </div>
                  @yield('fiter')

              </div>
          </div>
          @yield('content')


      </div>

      <!-- End Page wrapper  -->

  </div>

  @include('backend.layouts.partials.footer')