 @extends('backend.layouts.app')
 @section('breadcrumb')Add Role @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item"><a href="{{route('roles.index')}}" class="text-muted">Roles</a></li>
 <li class="breadcrumb-item text-muted active" aria-current="page">Create</li>
 @endsection
 @section('content')

 <div class="container-fluid">

     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-plus-square-o fa-lg"></i>
                     <strong>Create Role</strong>
                 </div>
                 <div class="card-body">
                     {!! Form::open(['route' => 'roles.store']) !!}

                     @include('backend.roles.fields')

                     {!! Form::close() !!}
                 </div>
             </div>
         </div>
     </div>

 </div>
 @endsection