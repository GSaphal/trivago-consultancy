 @extends('backend.layouts.app')
 @section('breadcrumb')Roles @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Roles</li>
 @endsection
 @section('content')

 <div class="container-fluid">

     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-align-justify"></i>
                     Roles

                     <a class="float-right" href="{!! route('roles.create') !!}"><i class="fa fa-plus-square fa-lg"></i></a>

                 </div>
                 <div class="card-body">
                     <div class="table-responsive-sm">
                         <table class="table table-striped">
                             <thead>
                                 <tr>
                                     <th>S.No</th>
                                     <th>Name</th>
                                     <th width="280px">Action</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 @foreach ($roles as $key => $role)
                                 <tr>
                                     <td>{{ ++$i }}</td>
                                     <td>{{ $role->name }}</td>
                                     <td>

                                         <a href="{!! route('roles.edit', [$role->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>

                                         {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                         {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                         {!! Form::close() !!}

                                     </td>
                                 </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     </div>

 </div>

 @endsection