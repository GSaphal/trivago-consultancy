@extends('backend.layouts.app')
@section('breadcrumb')
Student School
@endsection
@section('breadcrumb-item')
<li class="breadcrumb-item text-muted active" aria-current="page">Student School</li>
@endsection
@section('content')

<div class="container-fluid">

    <div class="row">
        @foreach($schools as $key => $school)
        <div class="col-lg-4 col-xlg-3 col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://photos.applyboard.com/schools/000/000/583/logos/original/McGill_University.png?1522781446"
                                width="68px" height="68px">
                        </div>
                        <div class="col-md-8">
                            <div style="font-size:18px;"><a href="{{route('students.schoolView',$school->id)}}" style="color:black">{{$school->name}}</a>
                            </div>
                            <div style="font-size:16px;"> {{isset($school->schoolAddress->address)?$school->schoolAddress->address:''}},{{isset($school->schoolAddress->country)?$school->schoolAddress->country:''}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
        <!-- Column -->
    </div>

</div>

@endsection