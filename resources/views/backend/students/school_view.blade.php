@extends('backend.layouts.app')

@section('content')
<img src="https://images.unsplash.com/photo-1604134967494-8a9ed3adea0d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"
    alt="" style="width:100%;height:400px;">
<div class="container-fluid">
    {{-- <div style="position: fixed; z-index: -1; opacity: 0; transform: translateZ(0px); top: 0px; width: 354.3px;">
        <div>
            <div><img
                    src="https://photos.applyboard.com/schools/000/000/583/logos/original/McGill_University.png?1522781446"
                    alt="McGill University - School of Continuing Studies" data-testid="SchoolLogo"
                   ></div>
            <div class="card">
                <div class="card-body">
                    <div class="css-14fpje8 css-7ozpf3">
                        <div data-testid="SchoolProperties" class="css-0 css-7ozpf4">
                            <h1 data-testid="SchoolName" class="css-1v15s8u css-7ozpf5">McGill University - School of
                                Continuing Studies</h1>
                            <h3 class="css-1yiy1vp css-7ozpf6"><img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAk1BMVEUAAADPAADfAAD////XAADfAAD////cAADfAADcAAD////fAAD////dAAD////////dAAD////dAADfAAD////////dAAD////eAADfAAD////eAAD////eAAD////eAAD////eAADgEBDiHx/iICDkMDDmQEDqYGDscHDwkJDyn5/yoKDzn5/3v7/739/97+/////w+QqSAAAAIXRSTlMAEBAQICAgUF9gYH9/gICPkJCfn5+gr6+wv7/Pz9/f7++1dCAXAAABeklEQVR4AZXVhXLrMBCF4RMGW2U2nkIY9v1f7oKmtUcruVb+ocB+2SgIr9Hy+rmsRaR8u15O0dMofROnXBs1XovfQydJagmXINQ4o3TEYgyvecVuwGoGVUL+BshEz/cAJWbsB5yjaVzFgKo9ecEYwMw9QD+ggW3MWFANLLiLBjTtAg2ORwXaFYsgWK0UaFdkIXAkjyHw2jwjBVbkKgQ4wCIAPmj7DIAFbgJgTds6AG7wqsD6JHKgbSdyWinwgkIBcn060XY6b0gFCtAFZ5Kfe3uIz+07PVApcPyk03/tAGqw0mB1VKBygCJ2XAH30JZ88bsvO64O/eQBaXasxAOvuPE3sMnfcIOFB1ZsWnlggaEGNnWtbQK8XgIyAOYSsAAwrC4AEzgrRKfBPWBXRIMJbCYWGHyXxYECP03ifownaFrEAOdPyPQDAyfTBwxUs9//FBfwmhTdIJsglJFwtUFH08fQ+NUI3U2XuTi9+eO+uX0r/z9y+Xy79Kf/ApwsHnA9FA/JAAAAAElFTkSuQmCC"
                                    alt="McGill University - School of Continuing Studies"> Montreal, Quebec</h3>
                            <h3 data-testid="SubHeadings" class="css-dskh8v css-7ozpf7">School ID: 583 </h3>
                            <div class="__react_component_tooltip place-top type-dark" id="adminEdit" data-id="tooltip">Edit
                                School</div>
                            <h3 data-testid="SubHeadings" class="css-dskh8v css-7ozpf7">DLI#: O19359011033</h3>
                        </div>
                        <div class="css-1l5am2h css-7ozpf8">
                            <div class="mini css-0 css-1eaiwfn3" data-testid="SchoolFeatures">
                                <div class="css-1287nyr css-1eaiwfn4">
                                    <div class="css-13uxkf0 css-1eaiwfn0">
                                        <div data-testid="IconContainer" data-tip-disable="false"
                                            data-tip="Ineligible for Post Graduation Work Permit"
                                            class="css-1l49ilc css-1eaiwfn1" currentitem="false">
                                            <div class="checkbox check-not-available floating css-pn0d2s css-1w0z3wm0"><span
                                                    aria-hidden="true" class="fa fa-ban"></span></div><span
                                                aria-hidden="true" class="fa fa-graduation-cap"></span>
                                        </div>
                                        <div data-testid="Description" class="css-6syx3b css-1eaiwfn2">Post Graduation Work
                                            Permit <span aria-hidden="true" class="fa fa-info-circle"></span></div>
                                    </div>
                                    <div class="css-13uxkf0 css-1eaiwfn0">
                                        <div data-testid="IconContainer" data-tip-disable="false"
                                            data-tip="No Co-op / Internship Participation" class="css-1l49ilc css-1eaiwfn1"
                                            currentitem="false">
                                            <div class="checkbox check-not-available floating css-pn0d2s css-1w0z3wm0"><span
                                                    aria-hidden="true" class="fa fa-ban"></span></div><span
                                                aria-hidden="true" class="fa fa-suitcase"></span>
                                        </div>
                                        <div data-testid="Description" class="css-6syx3b css-1eaiwfn2">No Co-op / Internship
                                            Participation <span aria-hidden="true" class="fa fa-info-circle"></span></div>
                                    </div>
                                    <div class="css-13uxkf0 css-1eaiwfn0">
                                        <div data-testid="IconContainer" data-tip-disable="false"
                                            data-tip="Work While Studying" class="css-1l49ilc css-1eaiwfn1"
                                            currentitem="false">
                                            <div class="checkbox check-not-available floating css-pn0d2s css-1w0z3wm0"><span
                                                    aria-hidden="true" class="fa fa-ban"></span></div><span
                                                aria-hidden="true" class="fa fa-usd"></span>
                                        </div>
                                        <div data-testid="Description" class="css-6syx3b css-1eaiwfn2">Work While Studying
                                            <span aria-hidden="true" class="fa fa-info-circle"></span></div>
                                    </div>
                                    <div class="css-13uxkf0 css-1eaiwfn0">
                                        <div data-testid="IconContainer" data-tip-disable="false"
                                            data-tip="Conditional Offer Letter" class="css-1l49ilc css-1eaiwfn1"
                                            currentitem="false">
                                            <div class="checkbox check-not-available floating css-pn0d2s css-1w0z3wm0"><span
                                                    aria-hidden="true" class="fa fa-ban"></span></div><span
                                                aria-hidden="true" class="fa fa-envelope"></span>
                                        </div>
                                        <div data-testid="Description" class="css-6syx3b css-1eaiwfn2">Conditional Offer
                                            Letter <span aria-hidden="true" class="fa fa-info-circle"></span></div>
                                    </div>
                                    <div class="css-13uxkf0 css-1eaiwfn0">
                                        <div data-testid="IconContainer" data-tip-disable="false" data-tip="Accommodations"
                                            class="css-1l49ilc css-1eaiwfn1" currentitem="false">
                                            <div class="checkbox check-available floating css-pn0d2s css-1w0z3wm0"><span
                                                    aria-hidden="true" class="fa fa-check"></span></div><span
                                                aria-hidden="true" class="fa fa-home"></span>
                                        </div>
                                        <div data-testid="Description" class="css-6syx3b css-1eaiwfn2">Accommodations <span
                                                aria-hidden="true" class="fa fa-info-circle"></span></div>
                                    </div>
                                </div>
                                <div class="__react_component_tooltip place-top type-dark" data-id="tooltip"></div>
                            </div>
                        </div>
                        <div class="css-0 css-7ozpf12">
                            <div class="css-hjuk2m css-7ozpf13">
                                <p class="css-1uk1gs8 css-7ozpf14"><span aria-hidden="true"
                                        class="fa fa-certificate"></span><b> Founded:</b> 1968</p>
                            </div>
                            <div class="css-hjuk2m css-7ozpf13">
                                <p class="css-1uk1gs8 css-7ozpf14"><span aria-hidden="true"
                                        class="fa fa-university"></span><b> Type:</b> Public, English Institute</p>
                            </div>
                            <div class="css-hjuk2m css-7ozpf13">
                                <p class="css-1uk1gs8 css-7ozpf14"><span aria-hidden="true" class="fa fa-group"></span><b>
                                        Total Students:</b> 12500+</p>
                            </div>
                        </div>
                        <div class="css-1sjxjpz css-7ozpf15"><a data-testid="BrowsePrograms"
                                class="css-1otcmok css-7ozpf16">Browse Programs</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-center">
                    <div style="font-size:28px">
                        {{$school->name}}
                    </div>
                    <div style="font-size:18px;">
                        {{isset($school->schoolAddress->address)?$school->schoolAddress->address:''}},{{isset($school->schoolAddress->country)?$school->schoolAddress->country:''}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>About</h3>
                </div>

                <div class="card-body">
                    <div style="font-size:16px">
                        {{$school->description}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection