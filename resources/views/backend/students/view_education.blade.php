@extends('backend.layouts.app')

@section('breadcrumb')
Student Profile
@endsection
@section('breadcrumb-item')
<li class="breadcrumb-item text-muted active" aria-current="page">Profile</li>
@endsection
@section('content')

<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12 col-xlg-12 col-md-12">
            @include('backend.students.profile_status')
            <div class="card mt-3">
                <!-- Tabs -->
                @include('backend.students.tab')
                <!-- Tabs -->
                <div class="tab-content">
                    <div class="card-body">
                        {!! Form::open(['route' => 'students.storeEducation','class'=>'form-horizontal form-material'])
                        !!}
                        @if(isset( $studentEducations))
                        @foreach($studentEducations as $key=> $studentEducation)
                        <div class="multi-field-wrapper">
                            <div class="multi-fields">
                                <div class="multi-field">
                                    <div class="form-group">
                                        <div class="row">
                                            {!! Form::hidden('id[]',
                                            isset($studentEducation)?$studentEducation->id:null, ['class' =>
                                            'form-control']) !!}
                                            <div class="col-md-2">
                                                <label>Country</label>
                                                {!! Form::text('education_country[]',
                                                isset($studentEducation)?$studentEducation->education_country:null,
                                                ['class' => 'form-control form-control-line']) !!}
                                            </div>

                                            <div class="col-md-2">
                                                <label>Level</label>
                                                {!! Form::text('level[]',
                                                isset($studentEducation)?$studentEducation->level:null, ['class' =>
                                                'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Faculty</label>
                                                {!! Form::text('faculty[]',
                                                isset($studentEducation)?$studentEducation->faculty:null, ['class' =>
                                                'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School Name</label>
                                                {!! Form::text('name[]',
                                                isset($studentEducation)?$studentEducation->name:null, ['class' =>
                                                'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School Address</label>
                                                {!! Form::text('school_address[]',
                                                isset($studentEducation)?$studentEducation->school_address:null,
                                                ['class' => 'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School City</label>
                                                {!! Form::text('school_city[]',
                                                isset($studentEducation)?$studentEducation->school_city:null, ['class'
                                                => 'form-control form-control-line']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <label>From Date</label>
                                                {!! Form::date('from_date[]',
                                                isset($studentEducation)?$studentEducation->from_date:null, ['class' =>
                                                'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-3">
                                                <label>To Date</label>
                                                {!! Form::date('to_date[]',
                                                isset($studentEducation)?$studentEducation->to_date:null, ['class' =>
                                                'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Grading Type</label>
                                                {!! Form::text('grading_type[]',
                                                isset($studentEducation)?$studentEducation->grading_type:null, ['class'
                                                => 'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Grading Value</label>
                                                {!! Form::text('grading_value[]',
                                                isset($studentEducation)?$studentEducation->grading_value:null, ['class'
                                                => 'form-control form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2 remove-btn mt-4">
                                                <button type="button" class="remove-field btn btn-danger">-</button>
                                            </div>

                                        </div>
                                    </div>





                                </div>
                            </div>
                            @if(count($studentEducations)==$key+1)
                            <button type="button" class="add-field btn btn-primary">ADD</button>
                            @endif

                        </div>
                        @endforeach
                        @else
                        <div class="multi-field-wrapper">
                            <div class="multi-fields">
                                <div class="multi-field">
                                    <div class="form-group">
                                        <div class="row">
                                            {!! Form::hidden('id[]', null, ['class' => 'form-control']) !!}
                                            <div class="col-md-2">
                                                <label>Country</label>
                                                {!! Form::text('education_country[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>

                                            <div class="col-md-2">
                                                <label>Level</label>
                                                {!! Form::text('level[]',null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Faculty</label>
                                                {!! Form::text('faculty[]',null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School Name</label>
                                                {!! Form::text('name[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School Address</label>
                                                {!! Form::text('school_address[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>School City</label>
                                                {!! Form::text('school_city[]',null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <label>From Date</label>
                                                {!! Form::date('from_date[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-3">
                                                <label>To Date</label>
                                                {!! Form::date('to_date[]',null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Grading Type</label>
                                                {!! Form::text('grading_type[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <label>Grading Value</label>
                                                {!! Form::text('grading_value[]', null, ['class' => 'form-control
                                                form-control-line']) !!}
                                            </div>
                                            <div class="col-md-2 remove-btn mt-4">
                                                <button type="button" class="remove-field btn btn-danger">-</button>
                                            </div>

                                        </div>
                                    </div>





                                </div>
                            </div>

                            <button type="button" class="add-field btn btn-primary">ADD</button>


                        </div>
                        @endif
                        <div class="form-group mt-4 float-right">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Next</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>

</div>

@endsection
@section('scripts')
<script>
    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
        });
        $('.multi-field .remove-field', $wrapper).click(function() {


            $(this).parent('.remove-btn').parent('.row').parent('.form-group').parent('.multi-field').remove();
        });
    });
</script>
@endsection