 @extends('backend.layouts.app')

 @section('breadcrumb')
 Student Profile
 @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Profile</li>
 @endsection
 @section('content')
 <!-- Container fluid  -->
 <!-- ============================================================== -->
 <div class="container-fluid">
     <!-- ============================================================== -->
     <!-- Start Page Content -->
     <!-- ============================================================== -->
     <!-- Row -->
     <div class="row">
         <div class="col-lg-12 col-xlg-12 col-md-12">
             @include('backend.students.profile_status')
             <div class="card mt-3">
                 <!-- Tabs -->
                 @include('backend.students.tab')
                 <!-- Tabs -->
                 <div class="tab-content">
                     <div class="card-body">
                         {!! Form::open(['route' => 'students.store','class'=>'form-horizontal form-material']) !!}

                         <div class="form-group">
                             <div class="row">

                                 <div class="col-md-4">
                                     <label>First Name</label>
                                     {!! Form::text('first_name', isset($student)?$student->first_name:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>

                                 <div class="col-md-4">
                                     <label>Middle Name</label>
                                     {!! Form::text('middle_name', isset($student)?$student->middle_name:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                                 <div class="col-md-4">
                                     <label>Last Name</label>
                                     {!! Form::text('last_name', isset($student)?$student->last_name:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                             </div>
                         </div>
                         <div class="form-group">
                             <div class="row">



                                 <div class="col-md-4">
                                     <label>Email</label>
                                     {!! Form::email('email_address', isset($student)?$student->email_address:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                                 <div class="col-md-4">
                                     <label>Home Number</label>
                                     {!! Form::text('home_number', isset($student)?$student->home_number:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                                 <div class="col-md-4">
                                     <label>Phone Number</label>
                                     {!! Form::text('phone_number', isset($student)?$student->phone_number:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>

                             </div>
                         </div>
                         <div class="form-group">
                             <div class="row">

                                 <div class="col-md-4">
                                     <label>Date Of Birth</label>
                                     {!! Form::date('dob', isset($student)?$student->dob:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                                 <div class="col-md-4">
                                     <label>Gender</label>
                                     <div class="col-md-8 col-sm-8 col-lg-8">

                                         <label class="radio-inline mr-3">
                                             {!! Form::radio('gender', "Male", isset($student)?(($student->gender=='Male')?true:false):false )!!}&nbsp;&nbsp;Male
                                         </label>

                                         <label class="radio-inline">
                                             {!! Form::radio('gender', "Female", isset($student)?(($student->gender=='Female')?true:false):false ) !!}&nbsp;&nbsp;Female
                                         </label><br>


                                     </div>
                                 </div>
                             </div>
                         </div>

                         <div class="form-group float-right">
                             <div class="col-sm-12">
                                 <button class="btn btn-success">Next</button>
                             </div>
                         </div>
                         {!! Form::close() !!}
                     </div>
                 </div>
             </div>
         </div>
         <!-- Column -->
     </div>
     <!-- Row -->
     <!-- ============================================================== -->
     <!-- End PAge Content -->
     <!-- ============================================================== -->
     <!-- ============================================================== -->
     <!-- Right sidebar -->
     <!-- ============================================================== -->
     <!-- .right-sidebar -->
     <!-- ============================================================== -->
     <!-- End Right sidebar -->
     <!-- ============================================================== -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 @endsection