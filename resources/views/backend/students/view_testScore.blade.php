 @extends('backend.layouts.app')

 @section('breadcrumb')
 Student Profile
 @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Profile</li>
 @endsection
 @section('content')

 <!-- Container fluid  -->
 <!-- ============================================================== -->
 <div class="container-fluid">
     <!-- ============================================================== -->
     <!-- Start Page Content -->
     <!-- ============================================================== -->
     <!-- Row -->
     <div class="row">
         <div class="col-lg-12 col-xlg-12 col-md-12">
             @include('backend.students.profile_status')
             <div class="card mt-3">
                 <!-- Tabs -->
                 @include('backend.students.tab')
                 <!-- Tabs -->
                 <div class="tab-content">
                     <div class="card-body">
                         {!! Form::open(['route' => 'students.storeTestScore','class'=>'form-horizontal form-material']) !!}

                         <div class="form-group">
                             <div class="row">

                                 <div class="col-md-4">
                                     <label>Test Name</label>
                                     {!! Form::text('test_name', isset($studentTestScore)?$studentTestScore->test_name:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>

                                 <div class="col-md-4">
                                     <label>Test Date</label>
                                     {!! Form::date('test_date', isset($studentTestScore)?$studentTestScore->test_date:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                                 <div class="col-md-4">
                                     <label>Test Score</label>
                                     {!! Form::text('test_score', isset($studentTestScore)?$studentTestScore->test_score:null, ['class' => 'form-control form-control-line']) !!}
                                 </div>
                             </div>
                         </div>



                         <div class="form-group float-right">
                             <div class="col-sm-12">
                                 <button class="btn btn-success">Finish</button>
                             </div>
                         </div>
                         {!! Form::close() !!}
                     </div>
                 </div>
             </div>
         </div>
         <!-- Column -->
     </div>
     <!-- Row -->
     <!-- ============================================================== -->
     <!-- End PAge Content -->
     <!-- ============================================================== -->
     <!-- ============================================================== -->
     <!-- Right sidebar -->
     <!-- ============================================================== -->
     <!-- .right-sidebar -->
     <!-- ============================================================== -->
     <!-- End Right sidebar -->
     <!-- ============================================================== -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 @endsection