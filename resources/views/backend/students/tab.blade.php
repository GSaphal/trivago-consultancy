 <ul class="nav nav-pills custom-pills">
     <li class="nav-item">
         <a href="{{route('students.index')}}" class="nav-link {{ Route::is('students.index') ? 'active' : '' }}">General Information</a>
     </li>
     <li class="nav-item">
         <a href="{{route('students.viewAddress')}}" class="nav-link {{ Route::is('students.viewAddress') ? 'active' : '' }}">Address Info</a>
     </li>
     <li class="nav-item">
         <a href="{{route('students.viewEducation')}}" class="nav-link {{ Route::is('students.viewEducation') ? 'active' : '' }}">Education History</a>
     </li>
     <li class="nav-item">
         <a href="{{route('students.viewTestScore')}}" class="nav-link {{ Route::is('students.viewTestScore') ? 'active' : '' }}">Test Score</a>
     </li>
 </ul>