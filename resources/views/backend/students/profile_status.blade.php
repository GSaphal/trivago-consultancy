 <?php

    use App\Models\Student;
    use Illuminate\Support\Facades\Auth;

    $profile_status = 0;
    $studentData = Student::where('user_id', Auth::user()->id)->first();
    if (!empty($studentData)) {
        $profile_status = ($studentData->profile_status / 40) * 100;
    }
    ?>
 <small>Profile Status</small>
 <div class="float-right">{{$profile_status}}% <i class="fas fa-level-up-alt text-success"></i>
 </div>
 <div class="progress">
     <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $profile_status; ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
 </div>