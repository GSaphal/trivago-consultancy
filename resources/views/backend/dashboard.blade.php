 @extends('backend.layouts.app')


 @section('content')
 <!-- Bread crumb and right sidebar toggle -->
 <!-- ============================================================== -->
 <div class="page-breadcrumb">
     <div class="row">
         <div class="col-7 align-self-center">
             <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Good Morning {{Auth::user()->name}}</h3>
             <div class="d-flex align-items-center">
                 <nav aria-label="breadcrumb">
                     <ol class="breadcrumb m-0 p-0">
                         <li class="breadcrumb-item"><a href="/home" class="text-muted">Dashboard</a>
                         </li>
                     </ol>
                 </nav>
             </div>
         </div>
  
     </div>
 </div>
 <!-- ============================================================== -->
 <!-- End Bread crumb and right sidebar toggle -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- Container fluid  -->
 <!-- ============================================================== -->
 <div class="container-fluid">
     <!-- *************************************************************** -->
     <!-- Start Earnings & Carousel Widget -->
     <!-- *************************************************************** -->
     <div class="row align-items-stretch">
  
     </div>
     <!-- *************************************************************** -->
     <!-- End Earnings & Carousel Widget -->
     <!-- *************************************************************** -->
     <!-- *************************************************************** -->
     <!-- Start Total Sales & Earnings Chart -->
  
     <!-- *************************************************************** -->
     <!-- End Total Sales & Earnings Chart -->
     <!-- *************************************************************** -->
     <!-- *************************************************************** -->
     <!-- Start Top Leader Table -->
     <!-- *************************************************************** -->

     <!-- *************************************************************** -->
     <!-- End Top Leader Table -->
     <!-- *************************************************************** -->
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->

 @endsection