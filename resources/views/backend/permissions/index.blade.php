 @extends('backend.layouts.app')
 @section('breadcrumb')Permissions @endsection
 @section('breadcrumb-item')
 <li class="breadcrumb-item text-muted active" aria-current="page">Permissions</li>
 @endsection
 @section('content')
 <div class="container-fluid">

     <div class="row">
         <div class="col-lg-12">
             <div class="card">
                 <div class="card-header">
                     <i class="fa fa-align-justify"></i>
                     Permissions
                     <a class="float-right" href="{!! route('permissions.create') !!}"><i class="fa fa-plus-square fa-lg"></i></a>
                 </div>
                 <div class="card-body">
                     <div class="table-responsive-sm">
                         <table class="table table-striped">
                             <thead>
                                 <th>S.NO</th>
                                 <th>Name</th>
                                 <th colspan="3">Action</th>
                             </thead>
                             <tbody>
                                 @foreach($permissions as $permission)
                                 <tr>
                                     <td>{{++$i}}</td>
                                     <td>{!! $permission->name !!}</td>
                                     <td>
                                         {!! Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'delete']) !!}
                                         <div class='btn-group'>
                                             {{-- <a href="{!! route('permissions.show', [$permission->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>--}}
                                             <a href="{!! route('permissions.edit', [$permission->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                             {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                         </div>
                                         {!! Form::close() !!}
                                     </td>
                                 </tr>
                                 @endforeach
                             </tbody>
                         </table>
                     </div>
                     <div class="pull-right mr-3">

                     </div>
                 </div>
             </div>
         </div>
     </div>

 </div>
 @endsection