@extends('backend.layouts.app')
@section('breadcrumb') Edit Permission @endsection
@section('breadcrumb-item')
<li class="breadcrumb-item"><a href="{{route('permissions.index')}}" class="text-muted">Permissions</a></li>
<li class="breadcrumb-item text-muted active" aria-current="page">Edit</li>
@endsection
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit fa-lg"></i>
                    <strong>Edit Permission</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method' => 'patch']) !!}

                    @include('backend.permissions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection