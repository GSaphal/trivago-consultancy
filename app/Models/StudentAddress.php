<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentAddress extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'student_id',
        'country',
        'address',
        'city',
        'province',
        'zip_code'


    ];
    public function students()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
