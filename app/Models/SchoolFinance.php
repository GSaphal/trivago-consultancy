<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolFinance extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['tution_fee', 'admission_fee', 'annual_charges', 'school_id'];
    public function schools()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
}
