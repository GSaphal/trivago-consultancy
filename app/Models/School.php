<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'school_type',
        'total_students',
        'logo',
        'description',
        'user_id'

    ];
    public function schoolAddress()
    {

        return $this->hasOne(SchoolAddress::class, 'school_id');
    }
    public function schoolFinance()
    {

        return $this->hasOne(SchoolFinance::class, 'school_id');
    }
    public function schoolImages()
    {

        return $this->hasMany(SchoolImage::class, 'school_id');
    }

    public function schoolFeatures()
    {

        return $this->hasMany(SchoolFeature::class, 'school_id');
    }
    public function schoolCourse()
    {

        return $this->hasMany(SchoolCourse::class, 'school_id');
    }
}
