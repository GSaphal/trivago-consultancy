<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolAddress extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'address', 'city', 'country', 'province', 'zip_code', 'school_id'
    ];

    public function schools()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
}
