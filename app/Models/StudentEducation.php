<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentEducation extends Model
{
    use HasFactory;
    protected $fillable = [
        'student_id',
        'level',
        'education_country',
        'faculty',
        'name',
        'from_date',
        'to_date',
        'grading_type',
        'grading_value',
        'school_address',
        'school_city'

    ];
    public function students()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
