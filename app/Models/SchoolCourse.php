<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolCourse extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['course_name', 'start_date', 'application_fee', 'school_id'];
    public function schools()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
}
