<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'dob',
        'gender',
        'email_address',
        'home_number',
        'phone_number',
        'profile_status',
        'user_id'


    ];
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
   
}
