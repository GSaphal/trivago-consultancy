<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolImage extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['image', 'title', 'school_id'];
    public function schools()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
}
