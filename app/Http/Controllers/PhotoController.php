<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SchoolImage;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class PhotoController extends Controller
{

    public function upload(Request $request)
    {

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $imageName = $file->getClientOriginalName();

            $file->move($path = storage_path('app/public/schools'), $imageName);
            $upload = SchoolImage::create([
                'image' => $imageName,
                'school_id' => $request->school_id
            ]);
        }
    }



    public function delete(Request $request)
    {
        $filename =  $request->get('filename');
        SchoolImage::where('image', $filename)->delete();
        $path = storage_path('app/public/schools/') . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }

    public function destroy($data)
    {
        $filename =  $data;
        SchoolImage::where('image', $filename)->delete();
        $path = storage_path('app/public/schools/') . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return redirect()->back()->with('danger', 'Image deleted successfully');
    }
}
