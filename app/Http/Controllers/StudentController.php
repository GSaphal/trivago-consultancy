<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Student;
use App\Models\StudentAddress;
use App\Models\StudentEducation;
use App\Models\StudentTest;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
    }

    public function index(Request $request)
    {
        $student = Student::where('user_id', Auth::user()->id)->first();
        if (!empty($student)) {
            return view('backend.students.index', compact('student'));
        }
        return view('backend.students.index');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email_address' => 'required',
            'home_number' => 'required',
            'phone_number' => 'required',
            'dob' => 'required',
            'gender' => 'required'

        ]);
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $student = Student::where('user_id', Auth::user()->id)->first();
        if (!empty($student)) {
            $student->update($input);
            Flash::success('Student General Info Updated successfully.');
            return redirect(route('students.viewAddress'));
        }
        $input['profile_status'] = 10;
        $student = Student::create($input);

        Flash::success('Student General Info Saved successfully.');

        return redirect(route('students.viewAddress'));
    }
    public function viewAddress(Request $request)
    {
        $student = Student::where('user_id', Auth::user()->id)->first();
        if (empty($student)) {
            Flash::error('First Fill Up General Information.');
            return view('backend.students.index');
        } else {
            $studentAddress = StudentAddress::where('student_id', $student->id)->first();
            if (!empty($studentAddress)) {
                return view('backend.students.view_address', compact('studentAddress'));
            }

            return view('backend.students.view_address');
        }
    }
    public function storeAddress(Request $request)
    {
        $this->validate($request, [
            'country' => 'required|max:120',
            'city' => 'required',
            'address' => 'required',
            'province' => 'required',
            'zip_code' => 'required'
        ]);
        $student = Student::where('user_id', Auth::user()->id)->first();
        $input = $request->all();
        $input['student_id'] = $student->id;
        $studentAddress = StudentAddress::where('student_id', $student->id)->first();
        if (!empty($studentAddress)) {
            $studentAddress->update($input);
            Flash::success('Student Address Info Updated successfully.');
            return redirect(route('students.viewEducation'));
        }

        StudentAddress::create($input);
        $student->profile_status += 10;
        $student->save();
        Flash::success('Student Address Info Saved successfully.');
        // Flash::success('Student saved successfully.');

        return redirect(route('students.viewEducation'));
    }

    public function viewEducation(Request $request)
    {
        $student = Student::where('user_id', Auth::user()->id)->first();
        if (empty($student)) {
            Flash::error('First Fill Up General Information.');
            return view('backend.students.index');
        } else {
            $studentEducations = StudentEducation::where('student_id', $student->id)->get();
            if (count($studentEducations) != 0) {
                return view('backend.students.view_education', compact('studentEducations'));
            }

            return view('backend.students.view_education');
        }
    }
    public function storeEducation(Request $request)
    {
        $this->validate($request, [
            'education_country' => 'required|array',
            'education_country.*' => 'required',
            'level.*' => 'required',
            'faculty.*' => 'required',
            'name.*' => 'required',
            'from_date.*' => 'required',
            'to_date.*' => 'required',
            'grading_type.*' => 'required',
            'grading_value.*' => 'required',
            'school_address.*' => 'required',
            'school_city.*' => 'required'
        ]);
        $student = Student::where('user_id', Auth::user()->id)->first();
        $studentEduId = StudentEducation::where('student_id', $student->id)->pluck('id')->toArray();

        $inputs = $request->except('_token');

        StudentEducation::whereIn('id', array_diff($studentEduId, $inputs['id']))->delete();

        foreach ($inputs['id'] as $key => $input) {
            $studentEducation = StudentEducation::where('id', $inputs['id'][$key])
                ->where('student_id', $student->id)->first();

            $data = array_combine(array_keys($inputs), array_column($inputs, $key));
            unset($data['id']);
            $data['student_id'] = $student->id;

            if (!empty($studentEducation)) {

                $studentEducation->update($data);
            } else {

                StudentEducation::create($data);
            }
        }
        if (count($studentEduId) == 0) {
            $student->profile_status += 10;
            $student->save();
        }


        Flash::success('Student Education Info Saved successfully.');

        return redirect(route('students.viewTestScore'));
    }

    public function viewTestScore(Request $request)
    {
        $student = Student::where('user_id', Auth::user()->id)->first();
        if (empty($student)) {
            Flash::error('First Fill Up General Information.');
            return view('backend.students.index');
        } else {
            $studentTestScore = StudentTest::where('student_id', $student->id)->first();
            if (!empty($studentTestScore)) {
                return view('backend.students.view_testScore', compact('studentTestScore'));
            }

            return view('backend.students.view_testScore');
        }
    }
    public function storeTestScore(Request $request)
    {
        $this->validate($request, [
            'test_name' => 'required|max:120',
            'test_date' => 'required',
            'test_score' => 'required'
        ]);
        $student = Student::where('user_id', Auth::user()->id)->first();
        $input = $request->all();
        $input['student_id'] = $student->id;
        $studentTestScore = StudentTest::where('student_id', $student->id)->first();
        if (!empty($studentTestScore)) {
            $studentTestScore->update($input);
            Flash::success('Student Test Score Info Updated successfully.');
            return redirect(route('students.index'));
        }


        StudentTest::create($input);
        $student->profile_status += 10;
        $student->save();

        Flash::success('Student Test Score Info Saved successfully.');

        return redirect(route('students.index'));
    }

    public function schoolIndex(){
        $schools=School::all();
        return view('backend.students.school_index',compact('schools'));

    }
    public function SchoolView($school){
        $school=School::find($school);
        
        return view('backend.students.school_view',compact('school'));

    }
}
