<?php

namespace App\Http\Controllers\Apply;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentApply;
use App\Models\StudentEducation;
class StudentApplyController extends Controller
{
    public function create($school_id){
        return view ('backend.apply');
    }
    public function store(Request $request, $school_id)
    {
        $student = Student::find(auth()->user()->id);
        // return $student->id;
        $student_education = StudentEducation::where('student_id', $student->id)->first();
        // return $student_education->grading_value;
        if($student_education->grading_type == 'Gpa' && $student_education->grading_value >=1.5)
        {
            $apply = new StudentApply();
            $apply->student_id = auth()->user()->id;
            $apply->school_id = $school_id;
            $apply->save();
            return redirect()->back();
        }
        elseif($student_education->grading_type == 'Percentage' && $student_education->grading_value >=60)
        {
            $apply = new StudentApply();
            $apply->student_id = auth()->user()->id;
            $apply->school_id = $school_id;
            $apply->save();
            return redirect()->back();
        }
        else{
            return "Not Eligible";
        }
       
    }
}
