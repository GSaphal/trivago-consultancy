<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\SchoolAddress;
use App\Models\SchoolCourse;
use App\Models\SchoolFeature;
use App\Models\SchoolFinance;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
    }
    public function index()
    {
        $schools = School::orderBy('created_at', 'asc')->get();
        return view('backend.schools.index', compact('schools'));
    }
    public function create()
    {
        return view('backend.schools.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'school_type' => 'required',
            'total_students' => 'required|integer',
            'description' => 'required',
            'tution_fee' => 'required|integer',
            'admission_fee' => 'required|integer',
            'annual_charges' => 'required|integer',
            'country' => 'required',
            'province' => 'required',
            'city' => 'required',
            'address' => 'required',
            'zip_code' => 'required'

        ]);
        $input = $request->all();
        if ($request->hasFile('logo')) {
            $input['logo']  = request('logo')->store('schools');
        }
        $school = School::create($input);
        if ($school) {
            $input['school_id'] = $school->id;
            SchoolFinance::create($input);
            SchoolAddress::create($input);
        }

        Flash::success('School General Info Saved successfully.');

        return redirect(route('schools.create'));
    }

    public function createCourse($id)
    {
        $school = School::find($id);
        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }
        $schoolCourses = SchoolCourse::where('school_id', $id)->get();
        return view('backend.schools.courses.index', compact('school', 'schoolCourses'));
    }
    public function storeCourse(Request $request)
    {

        $this->validate($request, [
            'course_name' => 'required|array',
            'course_name.*' => 'required',
            'start_date.*' => 'required',
            'application_fee.*' => 'required',
        ]);
        $school = School::where('id', $request->school_id)->first();
        $schCourseId = SchoolCourse::where('school_id', $request->school_id)->pluck('id')->toArray();

        $inputs = $request->except('_token', 'school_id');

        SchoolCourse::whereIn('id', array_diff($schCourseId, $inputs['id']))->delete();

        foreach ($inputs['id'] as $key => $input) {
            $schoolCourse = SchoolCourse::where('id', $inputs['id'][$key])
                ->where('school_id', $request->school_id)->first();

            $data = array_combine(array_keys($inputs), array_column($inputs, $key));
            unset($data['id']);
            $data['school_id'] = $request->school_id;

            if (!empty($schoolCourse)) {

                $schoolCourse->update($data);
            } else {

                SchoolCourse::create($data);
            }
        }



        Flash::success('School Courses  Saved successfully.');

        return redirect(route('schools.index'));
    }

    public function createFeature($id)
    {
        $school = School::find($id);
        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }
        $schoolFeatures = SchoolFeature::where('school_id', $id)->get();
        return view('backend.schools.features.index', compact('school', 'schoolFeatures'));
    }
    public function storeFeature(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|array',
            'name.*' => 'required',
            'description.*' => 'required',
        ]);
        $school = School::where('id', $request->school_id)->first();
        $schFeatureId = SchoolFeature::where('school_id', $request->school_id)->pluck('id')->toArray();

        $inputs = $request->except('_token', 'school_id');

        SchoolFeature::whereIn('id', array_diff($schFeatureId, $inputs['id']))->delete();

        foreach ($inputs['id'] as $key => $input) {
            $schoolFeature = SchoolFeature::where('id', $inputs['id'][$key])
                ->where('school_id', $request->school_id)->first();

            $data = array_combine(array_keys($inputs), array_column($inputs, $key));
            unset($data['id']);
            $data['school_id'] = $request->school_id;

            if (!empty($schoolFeature)) {

                $schoolFeature->update($data);
            } else {

                SchoolFeature::create($data);
            }
        }



        Flash::success('School Features  Saved successfully.');

        return redirect(route('schools.index'));
    }

    public function edit($id)
    {
        $school = School::find($id);

        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }

        return view('backend.schools.edit', compact('school'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'school_type' => 'required',
            'total_students' => 'required|integer',
            'description' => 'required',
            'tution_fee' => 'required|integer',
            'admission_fee' => 'required|integer',
            'annual_charges' => 'required|integer',
            'country' => 'required',
            'province' => 'required',
            'city' => 'required',
            'address' => 'required',
            'zip_code' => 'required'

        ]);
        $school = School::find($id);

        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }

        $input = $request->all();
        if ($request->hasFile('logo')) {
            $input['logo']  = request('logo')->store('schools');
        } else {
            $input['logo'] = $school->logo;
        }
        $school->update($input);
        $schoolAddress = SchoolAddress::where('school_id', $school->id)->first();
        if ($schoolAddress) {
            $schoolAddress->update($input);
        }
        $schoolFinance = SchoolFinance::where('school_id', $school->id)->first();
        if ($schoolFinance) {
            $schoolFinance->update($input);
        }

        Flash::success('School Info updated successfully.');

        return redirect(route('schools.index'));
    }
    public function destroy($id)
    {
        $school = School::find($id);

        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }

        $school->delete();

        Flash::success('School deleted successfully.');

        return redirect(route('schools.index'));
    }

    public function createImage($id)
    {
        $school = School::find($id);
        if (empty($school)) {
            Flash::error('School not found');

            return redirect(route('schools.index'));
        }
        return view('backend.schools.images.index', compact('school'));
    }

    public function show($id){
        $school= School::find($id)->first();
 
        return view('backend.schools.show', compact('school'));
    }
    public function findSchool(Request $request)
    {
        $schools = new School();
        $courses=SchoolCourse::all();
        $schoolName = $request->schoolName;
        if ($schoolName != null) {
            $names = explode(' ', $schoolName);
            $schools = $schools->where(function ($q) use ($names) {
                foreach ($names as $name) {
                    $q->where('name', 'like', "%{$name}%");
                }
            });
            $schools = $schools->where('name', $schoolName);
        }
        $schools = $schools->orderBy('created_at', 'asc')->get();
        return view('backend.schools.card.index', compact('schools', 'schoolName','courses'));
    }
}
